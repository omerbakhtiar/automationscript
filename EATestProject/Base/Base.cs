﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace EAAutoFramework.Base
{
    public class Base
    {

        private IWebDriver driver { get; set; }

        public BasePage CurrentPage
        {

            get
            {
                return (BasePage)ScenarioContext.Current["currentPage"];

            }
            set
            {

                ScenarioContext.Current["currentPage"] = value;
            }

        }


        public TPage GetInstance<TPage>() where TPage : BasePage, new()
        {
            TPage pageInsstance = new TPage()
            {
                driver = DriverContext.Driver

            };

            PageFactory.InitElements(DriverContext.Driver, pageInsstance);

            return pageInsstance;
        }


        public TPage As<TPage>() where TPage : BasePage
        {
            return (TPage)this;
        }
    }
}
