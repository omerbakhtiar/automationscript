﻿using EAAutoFramework.Config;
using EAAutoFramework.Helpers;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace EAAutoFramework.Base
{
    public abstract class BaseStep : Base
    {

        public virtual void NaviateSite()
        {
            
            DriverContext.Browser.GoToUrl(Settings.AUT);
            LogHelpers.Write("Opened the browser !!!");
        }

        public virtual void facebookLink()
        {

        
            DriverContext.Browser.GoToUrl("https://wwww.facebook.com");
        }

        public virtual void buisnessLink()
        {
            DriverContext.Browser.
                                GoToUrl("http://www.businessdirectory.pk/");
        }

        public virtual void lhrChamberUrl()
        {
            DriverContext.Browser.GoToUrl("http://www.lcci.com.pk/directory.php");
        }
    }
}
