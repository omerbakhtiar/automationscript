﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using System.Diagnostics;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    internal class ExtendedSteps : BaseStep
    {
        [Given(@"I have navigated to the application")]
        public void GivenIHaveNavigatedToTheApplication()
        {
            NaviateSite();
            CurrentPage = GetInstance<HomePage>();
        }

        [Given(@"I see application opened")]
        public void GivenISeeApplicationOpened()
        {
            CurrentPage.As<HomePage>().CheckIfLoginExist();
        }


        [Given(@"I close the popup")]
        public void GivenICloseThePopup()
        {
            CurrentPage.As<HomePage>().Close();
        }

        [Then(@"I click login link")]
        public void ThenIClickLoginLink()
        {
            CurrentPage.As<HomePage>().ClickLogin();
        }

        [Then(@"I close the application")]
        public void ThenICloseTheApplication()
        {
            CurrentPage = GetInstance<HomePage>();
            DriverContext.Driver.Close();
            DriverContext.Driver.Quit();
           
        }

        [Then(@"I click register link")]
        public void ThenIClickRegisterLink()
        {
            CurrentPage.As<HomePage>().registerClick();
            
        }

   
    }
}
