﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using NUnit.Framework;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace EAEmployeeTest.Steps
{
    [Binding]
    class CreateEmployeeStep : BaseStep
    {

        [Then(@"I am on the Register Page")]
        public void ThenIAmOnTheRegisterPage()
        {
            CurrentPage = GetInstance<CreateEmployeePage>();
            Thread.Sleep(5000);
            bool a = CurrentPage.As<CreateEmployeePage>().IsPresent();
            Console.WriteLine(a + "I have landed on Registeration page");
        }

        [Then(@"I enter invalid data in ""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"" and ""(.*)""")]
        public void ThenIEnterInvalidDataInAnd(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7)
        {
            Thread.Sleep(5000);
            CurrentPage.As<CreateEmployeePage>().CreateEmployee(p0, p1, p2, p3, p4, p5, p6, p7);
        }

        [Then(@"I validate email error message ""(.*)"" is displayed on employee registeration Page")]
        public void ThenIValidateEmailErrorMessageIsDisplayedOnEmployeeRegisterationPage(string p0)
        {
            bool ans=CurrentPage.As<CreateEmployeePage>().message(p0);
            Assert.True(ans);

        }

        [Then(@"I click on accept application")]
        public void ThenIClickOnAcceptApplication()
        {
            CurrentPage.As<CreateEmployeePage>().AcceptAggrement();
        }

        [Then(@"I submit the form")]
        public void ThenISubmitTheForm()
        {
            CurrentPage.As<CreateEmployeePage>().SubmitForm();
        }

        [Then(@"I should be Navigated to Verification Code Page and I enter vefication code there")]
        public void ThenIShouldBeNavigatedToVerificationCodePageAndIEnterVeficationCodeThere()
        {
            string s =CurrentPage.As<CreateEmployeePage>().connectToDB();
            CurrentPage.As<CreateEmployeePage>().verifyCode(s);
            Thread.Sleep(10000);
        }

        [Then(@"I submit the button")]
        public void ThenISubmitTheButton()
        {
            CurrentPage.As<CreateEmployeePage>().submitCode();
            Thread.Sleep(10000);
        }


    }
}
