﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public sealed class Employer:BaseStep
    {

        [Then(@"I click on buisness link")]
        public void ThenIClickOnBuisnessLink()
        {

            CurrentPage = CurrentPage.GetInstance<Employers>();
            Thread.Sleep(10000);
            CurrentPage.As<Employers>().clickEmp();
                
                
         }


        [Then(@"I enter ""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)""and""(.*)""")]
        public void ThenIEnterAnd(string p0, string p1, string p2, string p3, string p4, string p5)
        {
            Thread.Sleep(10000);
            CurrentPage.As<Employers>().employerData(p0, p1, p2, p3, p4, p5);
        }

        [Then(@"I click Next button")]
        public void ThenIClickNextButton()
        {
            CurrentPage.As<Employers>().clickFirstNext();
            Thread.Sleep(10000);
        }

        [Then(@"I select ""(.*)"" of the employer firm")]
        public void ThenISelectOfTheEmployerFirm(string p0)
        {
            ScenarioContext.Current.Pending();
        }


        [Then(@"I check that invalid accurate ""(.*)"" is appearing")]
        public void ThenICheckThatInvalidAccurateIsAppearing(string p0)
        {
            Thread.Sleep(10000);
           Assert.IsTrue(CurrentPage.As<Employers>().invalidMessage(p0));
      
        }

        [Then(@"BuisnessName is not displayed as page is not navigated forward")]
        public void ThenBuisnessNameIsNotDisplayedAsPageIsNotNavigatedForward()
        {
            Assert.IsFalse(CurrentPage.As<Employers>().isNotDisplayed());
            Thread.Sleep(10000);
        }

        [Then(@"I enter second form data ""(.*)"",""(.*)"",""(.*)"",""(.*)""and""(.*)""")]
        public void ThenIEnterSecondFormDataAnd(string p0, string p1, string p2, Decimal p3, string p4)
        {
            CurrentPage.As<Employers>().insertSecondFormData(p0, p1, p2, p3.ToString(), p4);

        }

        [Then(@"I click on second Next button")]
        public void ThenIClickOnSecondNextButton()
        {
            CurrentPage.As<Employers>().nextButtonOfForm();
        }

        [Then(@"I check that invalid accurate on second form ""(.*)"" is appearing")]
        public void ThenICheckThatInvalidAccurateOnSecondFormIsAppearing(string p0)
        {
            Assert.IsTrue(CurrentPage.As<Employers>().invalidSecondMessage(p0));
        }


        [Then(@"I click previous button")]
        public void ThenIClickPreviousButton()
        {
            Thread.Sleep(10000);
            CurrentPage.As<Employers>().previousClick();
        }

        [Then(@"I edit the fields of first form of the employer registeration")]
        public void ThenIEditTheFieldsOfFirstFormOfTheEmployerRegisteration()
        {
            CurrentPage.As<Employers>().clearFieldsFirstForm();
        }


        [Then(@"I enter the city ""(.*)""")]
        public void ThenIEnterTheCity(string p0)
        {
            CurrentPage.As<Employers>().selectLocation(p0);
        }

        [Then(@"I verify that ""(.*)"" ,""(.*)"" and ""(.*)"" is autoselected")]
        public void ThenIVerifyThatAndIsAutoselected(string p0, string p1, string p2)
        {
            Thread.Sleep(10000);
           Boolean a= CurrentPage.As<Employers>().stateIsSelected(p0, p1, p2);
            Assert.IsTrue(a);
        }

        [Then(@"I enter ""(.*)"",""(.*)"" and ""(.*)""")]
        public void ThenIEnterAnd(string p0, string p1, int p2)
        {
            CurrentPage.As<Employers>().insertAddressOne(p0);
            CurrentPage.As<Employers>().insertAddressTwo(p1);
            CurrentPage.As<Employers>().insertZipCode(p2);
        }

        [Then(@"I edit up the adress fields")]
        public void ThenIEditUpTheAdressFields()
        {
            CurrentPage.As<Employers>().clearAdress();
            Thread.Sleep(7000);
        }

        [Then(@"I renter the edit fields of adress ""(.*)"",""(.*)"" and ""(.*)""")]
        public void ThenIRenterTheEditFieldsOfAdressAnd(string p0, string p1, int p2)
        {
            CurrentPage.As<Employers>().insertAddressOne(p0);
            CurrentPage.As<Employers>().insertAddressTwo(p1);
            CurrentPage.As<Employers>().insertZipCode(p2);
        }

        [Then(@"I tick the acccept button")]
        public void ThenITickTheAccceptButton()
        {
            CurrentPage.As<Employers>().acceptAgreement();
        }


        [Then(@"I submit employer form")]
        public void ThenISubmitEmployerForm()
        {
            Thread.Sleep(5000);
            CurrentPage.As<Employers>().clickSubmit();
        }

        [Then(@"I enter up the code")]
        public void ThenIEnterUpTheCode()
        {
            CurrentPage = GetInstance<CreateEmployeePage>();
            Thread.Sleep(5000);
            string s = CurrentPage.As<CreateEmployeePage>().connectToDB();
            CurrentPage.As<CreateEmployeePage>().verifyCode(s);
            Thread.Sleep(10000);
        }

        [Then(@"I submit the button of the employer")]
        public void ThenISubmitTheButtonOfTheEmployer()
        {
            CurrentPage = GetInstance<Employers>();
            CurrentPage.GetInstance<Employers>().submitCd();
        }



    }
}
