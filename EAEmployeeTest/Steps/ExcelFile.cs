﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using EAAutoFramework.Base;

namespace EAEmployeeTest.Steps
{
    public class ExcelFile:BasePage
    {

        private string excelFilePath = "C:\\Users\\Dell\\Desktop\\AllScenarios\\EAEmployeeTest\\Data\\FetchData.xlsx";
       // private int rowNumber = 1; // define first row number to enter data in excel

        Microsoft.Office.Interop.Excel.Application myExcelApplication;
        Microsoft.Office.Interop.Excel.Workbook myExcelWorkbook;
        Microsoft.Office.Interop.Excel.Worksheet myExcelWorkSheet;

        public string ExcelFilePath
        {
            get { return excelFilePath; }
            set { excelFilePath = value; }
        }


        /*  public int Rownumber
          {
              get { return rowNumber; }
              set { rowNumber = value; }
          }*/

        public void openExcel()
        {
            myExcelApplication = null;

            myExcelApplication = new Microsoft.Office.Interop.Excel.Application(); // create Excell App
            myExcelApplication.DisplayAlerts = false; // turn off alerts


            myExcelWorkbook = (Microsoft.Office.Interop.Excel.Workbook)(myExcelApplication.Workbooks._Open(excelFilePath, System.Reflection.Missing.Value,
               System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value,
               System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value,
               System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value,
               System.Reflection.Missing.Value, System.Reflection.Missing.Value)); // open the existing excel file

            int numberOfWorkbooks = myExcelApplication.Workbooks.Count; // get number of workbooks (optional)

            myExcelWorkSheet = (Excel.Worksheet)myExcelWorkbook.Worksheets[3]; // define in which worksheet, do you want to add data
            //myExcelWorkSheet.Name = "Sheet7"; // define a name for the worksheet (optinal)

            int numberOfSheets = myExcelWorkbook.Worksheets.Count; // get number of Sheet1 (optional)
        }

        public void addDataToExcel(string title, string address, string phone, string email,int rowNumber)
        {

            myExcelWorkSheet.Cells[rowNumber, "A"] = title;
            myExcelWorkSheet.Cells[rowNumber, "B"] = address;
            myExcelWorkSheet.Cells[rowNumber, "C"] = phone;
            myExcelWorkSheet.Cells[rowNumber, "D"] = email;
            // if you put this method inside a loop, you should increase rownumber by one or wat ever is your logic
          
        }

        public void closeExcel()
        {
            try
            {
               
                myExcelWorkbook.SaveAs(excelFilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                        false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                myExcelWorkbook.Close();
            }
            finally
            {
                if (myExcelApplication != null)
                {
                    myExcelApplication.Quit(); // close the excel application
                }
            }

        }
    }
}
