﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public sealed class ExternalSearch:BaseStep
    {
        bool a;
        int num = 0;
        string s, value;
        [Given(@"I enter ""(.*)"" in external search")]
        public void GivenIEnterInExternalSearch(string p0)
        {
            CurrentPage = GetInstance<External>();
            CurrentPage.As<External>().insertText(p0);
        }

        [Given(@"I select ""(.*)"" from the drop down appearing on the page")]
        public void GivenISelectFromTheDropDownAppearingOnThePage(string p0)
        {
            CurrentPage.As<External>().selectCity(p0);
        }

        [Then(@"i select ""(.*)"" from the drop down on that page")]
        public void ThenISelectFromTheDropDownOnThatPage(string p0)
        {
            CurrentPage.As<External>().selectIndustry(p0);
        }

        [Then(@"I click on find job button to searcch the job")]
        public void ThenIClickOnFindJobButtonToSearcchTheJob()
        {
            CurrentPage = GetInstance<External>();
            CurrentPage.As<External>().clickFindJob();
        }

        [Then(@"I verify if no search result is found")]
        public void ThenIVerifyIfNoSearchResultIsFound()
        {
           
            Assert.IsTrue(CurrentPage.As<External>().IsNoSearchFound());
        }

    

        [Then(@"I click on the Job")]
        public void ThenIClickOnTheJob()
        {
            if (CurrentPage.As<External>().IsNoSearchFound() == true)
            {
                CurrentPage = GetInstance<External>();
                CurrentPage.As<External>().clickOnJob();
            }
        }

        [Then(@"I verify that ""(.*)"" is appearing as expected")]
        public void ThenIVerifyThatIsAppearingAsExpected(string p0)
        {

            Thread.Sleep(10000);
            CurrentPage.As<External>().checkSubClassification(p0);
        }


        [Then(@"I select ""(.*)"" and ""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"" and ""(.*)"" and click update search and verify results count")]
        public void ThenISelectAndAndAndClickUpdateSearchAndVerifyResultsCount(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8)
        {
            int num;
            Thread.Sleep(10000);
            value = CurrentPage.As<External>().clickMainClassification(p0, p1);
            Thread.Sleep(5000);
            CurrentPage.As<External>().clickUpdateButton();
            s = CurrentPage.As<External>().connectStoreProc(p0,p1,p2,p3,p4,p5,p6,p7,p8);
            Console.WriteLine(s);
            
            
        }

        [Then(@"I verify the count")]
        public void ThenIVerifyTheCount()
        {
            if (CurrentPage.As<External>().checkNotFound() == false)
            {
                Assert.IsTrue(value.Contains(s));
            }else
            {
                Assert.IsTrue(CurrentPage.As<External>().checkNotFound());
            }
        }

  
        [Then(@"I select again ""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"" and ""(.*)"" and ""(.*)""")]
        public void ThenISelectAgainAndAnd(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8, string p9)
        {
            int num;
            int m;
            Thread.Sleep(10000);
            num=CurrentPage.As<External>().clickMainClassificationNew(p0, p1);
            Thread.Sleep(5000);
            CurrentPage.As<External>().clickUpdateButton();
            s = CurrentPage.As<External>().connectStoreProc(p0, p1, p2, p3, p4, p5, p6, p7, p8);
            Console.WriteLine(s);
            m=Int32.Parse(s);
            Console.WriteLine(num + "the value of the num is");
            Assert.AreEqual(m, num);
           
        }
        [Then(@"I select again ""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"" and ""(.*)"" and ""(.*)"" of new checkExternal")]
        public void ThenISelectAgainAndAndOfNewCheckExternal(string p0, string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8, string p9)
        {
           
            int m;
            Thread.Sleep(10000);

            string[] parts = p0.Split(';');
            /*  for (int i = 0; i < parts.Count(); i++)
              {
                  num += CurrentPage.As<External>().clickMainMultipleClassificationNew(parts[i], p1);
              }*/

            /* CurrentPage.As<External>().selectGender(p3);
             CurrentPage.As<External>().selectAge(p4);
             CurrentPage.As<External>().selectMinimumEducation(p5);
             CurrentPage.As<External>().selectExperiance(p6);
             CurrentPage.As<External>().selectDateListed(p7);
             CurrentPage.As<External>().selectWorkType(p8);*/
            CurrentPage.As<External>().selectSalary();
            Thread.Sleep(5000);
            //CurrentPage.As<External>().clickUpdateButton();
            /*s = CurrentPage.As<External>().connectStoreProc(p0, p1, p2, p3, p4, p5, p6, p7, p8);
            Console.WriteLine(s);
            m = Int32.Parse(s);
            Console.WriteLine(num + "the value of the num is");
            Assert.AreEqual(m, num);*/
        }

    }
}
