﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public class LoginSteps : BaseStep
    {


        [When(@"I enter ""(.*)""and""(.*)""")]
        public void WhenIEnterAnd(string p0, string p1)
        {
            CurrentPage = GetInstance<LoginPage>();
            Thread.Sleep(9000);
            CurrentPage.As<LoginPage>().Login(p0, p1);

        }
        [When(@"I enter ""(.*)""and""(.*)"" with integer values")]
        public void WhenIEnterAndWithIntegerValues(string p0, string p1)
        {
            CurrentPage = GetInstance<LoginPage>();
            Thread.Sleep(9000);
            CurrentPage.As<LoginPage>().Login(p0, p1.ToString());
        }



        [Then(@"I am on not on homepage a i have entered invalid data")]
        public void ThenIAmOnNotOnHomepageAIHaveEnteredInvalidData()
        {
            Thread.Sleep(5000);
            CurrentPage = GetInstance<LoginPage>();
            Boolean a = CurrentPage.As<LoginPage>().logPage();
            Console.WriteLine(a+"I am at the HomePage");

            Boolean b = CurrentPage.As<LoginPage>().loginErrorMessage();
            Console.WriteLine(b+"Wrong Password Entered");

            Boolean c = CurrentPage.As<LoginPage>().loginErrorMessageCredentials();
            Console.WriteLine(c + "Please provide credentials");

        }

        [Then(@"I get usename and password from the excel file")]
        public void ThenIGetUsenameAndPasswordFromTheExcelFile()
        {
            CurrentPage = GetInstance<LoginPage>();
            Thread.Sleep(5000);
            CurrentPage.As<LoginPage>().CallExcel();
        }

    }
}
