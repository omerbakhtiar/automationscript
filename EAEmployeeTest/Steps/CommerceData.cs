﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public sealed class CommerceData:BaseStep
    {
     
        [Given(@"I have navigated to the application of the given URL of the lahore chamber of commerce")]
        public void GivenIHaveNavigatedToTheApplicationOfTheGivenURLOfTheLahoreChamberOfCommerce()
        {
            lhrChamberUrl();
        }

        [Then(@"I click on the advance button")]
        public void ThenIClickOnTheAdvanceButton()
        {

            Thread.Sleep(7000);
            CurrentPage = GetInstance<LahoreData>();
            CurrentPage.As<LahoreData>().clickOnAdvance();
        }


        [Then(@"I select the ""(.*)"" and ""(.*)"" as given by the user who is entering up the data")]
        public void ThenISelectTheAndAsGivenByTheUserWhoIsEnteringUpTheData(string p0, string p1)
        {

            Thread.Sleep(10000);
            CurrentPage.As<LahoreData>().selectBuisnessType();
            Thread.Sleep(10000);
            CurrentPage.As<LahoreData>().selectBuisnessSector(p1);

        }

        [Then(@"I click on the button")]
        public void ThenIClickOnTheButton()
        {
            Thread.Sleep(5000);
           // CurrentPage.As<LahoreData>().clickButton();
        }


    }
}