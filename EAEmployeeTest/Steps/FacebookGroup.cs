﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public sealed class FacebookGroup: BaseStep
    {
        [Given(@"I have navigated to the facebook application")]
        public void GivenIHaveNavigatedToTheFacebookApplication()
        {

            facebookLink();
        }

        [When(@"I enter ""(.*)""and""(.*)"" for the facebook login")]
        public void WhenIEnterAndForTheFacebookLogin(string p0, string p1)
        {
            CurrentPage = GetInstance<FacebookLogin>();
            DriverContext.Driver.Manage().Window.Maximize();
            CurrentPage.As<FacebookLogin>().enterData(p0, p1);
        }

        [Then(@"I search with the ""(.*)""")]
        public void ThenISearchWithThe(string p0)
        {
            Thread.Sleep(10000);
            CurrentPage.As<FacebookLogin>().searchGroups(p0);
        }

        [Then(@"I click on the search button")]
        public void ThenIClickOnTheSearchButton()
        {
            

        }

    }
}
