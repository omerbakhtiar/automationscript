﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public sealed class EmployeeHome: BaseStep
    {
        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef
        [Then(@"I am on the HomePage of the employee")]
        public void ThenIAmOnTheHomePageOfTheEmployee()
        {
            CurrentPage = GetInstance<EmployeeHomePage>();
            Thread.Sleep(5000);
            Boolean a= CurrentPage.As<EmployeeHomePage>().IsPresent();
            Console.WriteLine(a);
        }

      



    }
}
