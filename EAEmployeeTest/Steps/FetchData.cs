﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public sealed class FetchData:BaseStep
    {
        [Given(@"I have navigated to the application of the given URL of  http://www\.businessdirectory\.pk/")]
        public void GivenIHaveNavigatedToTheApplicationOfTheGivenURLOfHttpWww_Businessdirectory_Pk()
        {
            buisnessLink();
        }


        [Then(@"I select the ""(.*)"" as given by the user for the city of that place")]
        public void ThenISelectTheAsGivenByTheUserForTheCityOfThatPlace(string p0)
        {
            CurrentPage = GetInstance<EmailData>();
            CurrentPage.As<EmailData>().getCityName(p0);
        }

        /*     [Then(@"I select the name of the ""(.*)"" as given by the user")]
             public void ThenISelectTheNameOfTheAsGivenByTheUser(string p0,int sheet)
             {
                 Thread.Sleep(10000);
                 //CurrentPage.As<EmailData>().getCat(p0,sheet);


             }
             */


        [Then(@"I select the name of the ""(.*)"" as given by the user")]
        public void ThenISelectTheNameOfTheAsGivenByTheUser(string p0)
        {
            Thread.Sleep(10000);
            CurrentPage.As<EmailData>().getCat(p0);
            Thread.Sleep(10000);
        }



        [Then(@"I select the ""(.*)"" of the company")]
        public void ThenISelectTheOfTheCompany(string p0)
        {

            CurrentPage.As<EmailData>().getSub(p0);
            Thread.Sleep(10000);
          
        }



    }

}

