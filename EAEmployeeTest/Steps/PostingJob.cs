﻿using EAAutoFramework.Base;
using EAEmployeeTest.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace EAEmployeeTest.Steps
{
    [Binding]
    public sealed class PostingJob : BaseStep
    {


        [Then(@"I click on Post a job button")]
        public void ThenIClickOnPostAJobButton()
        {
            CurrentPage = GetInstance<PostAJob>();
            CurrentPage.As<PostAJob>().clickPostJob();
        }

        [Then(@"I click on continue button")]
        public void ThenIClickOnContinueButton()
        {
            CurrentPage.As<PostAJob>().clickContinue();
        }


        [Then(@"I select ""(.*)"" and ""(.*)""")]
        public void ThenISelectAnd(string p0, string p1)
        {
            CurrentPage.As<PostAJob>().selectJobType(p0);
            CurrentPage.As<PostAJob>().selectJobEdition(p1);
        }

        [Then(@"I enter shortDescription from excelfile")]
        public void ThenIEnterShortDescriptionFromExcelfile()
        {
            Thread.Sleep(5000);
            CurrentPage.As<PostAJob>().insertShortDescripton();
        }


    }
}
