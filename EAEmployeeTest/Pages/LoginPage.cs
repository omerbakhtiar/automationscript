﻿using EAAutoFramework.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using EAAutoFramework.Extensions;
using System;
using System.Threading;
using EAAutoFramework.Helpers;
using System.Data.SqlClient;

namespace EAEmployeeTest.Pages
{
    class LoginPage : BasePage
    {
        [FindsBy(How= How.Id, Using ="userName")]
        IWebElement txtUserName { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        IWebElement txtPassword { get; set; }

        [FindsBy(How = How.Id, Using = "submitSignIn")]
        IWebElement btnLogin { get; set; }

        [FindsBy(How = How.Id, Using = "sign-in-err-msg")]
        IWebElement errormessage;
        private string val;

        public void Login(string userName, string password)
        {
            txtUserName.SendKeys(userName);
            txtPassword.SendKeys(password);
            btnLogin.Click();
        }

    


        public HomePage ClickLoginButton()
        {
            btnLogin.Submit();
            return GetInstance<HomePage>();
        }


        public Boolean logPage()
        {
            Thread.Sleep(10000);
            string s = DriverContext.Driver.Url.ToString();
            if (DriverContext.Driver.Url.ToString().Contains("Home/SignIn"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean loginErrorMessage()
        {
            string s = errormessage.Text;
            if (errormessage.Text.Contains(" Username or Password is not correct"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean loginErrorMessageCredentials()
        {
            string s = errormessage.Text;
            if (errormessage.Text.Contains("Please provide credential"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CallExcel()
        {
            string fileName = Environment.CurrentDirectory.ToString() + "\\Data\\Login.xlsx";
            ExcelHelpers.PopulateInCollection(fileName);
            Login(ExcelHelpers.ReadData(1, "UserName"), ExcelHelpers.ReadData(1, "Password"));
           
        }



    }
}
