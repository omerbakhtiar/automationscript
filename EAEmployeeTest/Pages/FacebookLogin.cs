﻿using EAAutoFramework.Base;
using EAAutoFramework.Helpers;
using Microsoft.Office.Interop.Excel;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EAEmployeeTest.Pages
{
    public class FacebookLogin : BasePage
    {
        [FindsBy(How = How.Id, Using = "email")]
        IWebElement userName { get; set; }

        [FindsBy(How = How.Id, Using = "pass")]
        IWebElement password { get; set; }

        [FindsBy(How = How.ClassName, Using = "uiButtonConfirm")]
        IWebElement button { get; set; }

        [FindsBy(How = How.ClassName, Using = "_1frb")]
        IWebElement searchValue { get; set; }


        [FindsBy(How = How.ClassName, Using = "_51sy")]
        IWebElement searchButton { get; set; }

        public void enterData(string email, string pass)
        {
            Thread.Sleep(7000);
            userName.SendKeys(email);
            password.SendKeys(pass);
            button.FindElement(By.TagName("input")).Click();


        }

        public void searchGroups(string value)
        {
            Thread.Sleep(7000);
            OpenQA.Selenium.Interactions.Actions actions = new OpenQA.Selenium.Interactions.Actions(DriverContext.Driver);
            actions.SendKeys(OpenQA.Selenium.Keys.Escape).Build().Perform();
            Thread.Sleep(10000);
            searchValue.SendKeys(value);
            Thread.Sleep(7000);
            DriverContext.Driver.FindElement(By.XPath("//button[@value='1']")).Click();
            Thread.Sleep(10000);
            DriverContext.Driver.FindElement(By.LinkText("Jobs In Lahore & Business")).Click();
            Thread.Sleep(10000);
            //IList<IWebElement> ele=DriverContext.Driver.FindElement(By.Id("pagelet_group_composer")).FindElements(By.TagName("div"));
            //IWebElement elet=ele[0].FindElement(By.TagName("div")).FindElement(By.TagName("div"));
            readData();
       
        }

        public void tabbing(int value)
        {
            // CallExcel();
            //  string s = ExcelHelpers.ReadData(value, "1");
            //s = s.Replace("  ", string.Empty).Replace("'", string.Empty).Replace("\n", " ");

          //  DriverContext.Driver.FindElement(By.XPath("//*[@data-testid='status-attachment-mentions-input']")).SendKeys(s);

            Thread.Sleep(10000);
            DriverContext.Driver.FindElement(By.XPath("//*[@data-testid ='react-composer-post-button']")).Click();
            Thread.Sleep(10000);
        }

        public void CallExcel()
        {
            string fileName = Environment.CurrentDirectory.ToString() + "\\Data\\Links.xlsx";
            ExcelHelpers.PopulateInCollection(fileName);


        }


        public void readData()
        {
            string fileName = Environment.CurrentDirectory.ToString() + "\\Data\\Links.xlsx";
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(fileName);
            Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;



            for (int i = 2; i <= rowCount; i++)
            {
                for (int j = 1; j <= colCount; j++)
                {

                    if (xlRange.Cells[i, j] != null && xlRange.Cells[i, j].Value2 != null)
                    {
                        string s = xlRange.Cells[i, j].Value2.ToString();
                        string flag = xlRange.Cells[i, j].Value3.ToString();
                        DriverContext.Driver.FindElement(By.XPath("//*[@data-testid='status-attachment-mentions-input']")).SendKeys(s);
                        Thread.Sleep(10000);
                        DriverContext.Driver.FindElement(By.XPath("//*[@data-testid ='react-composer-post-button']")).Click();
                        Thread.Sleep(10000);
                    }
                   
                }
            }
        }
    }
}