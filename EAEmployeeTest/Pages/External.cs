﻿using EAAutoFramework.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace EAEmployeeTest.Pages
{
    public class External : BasePage
    {
        private int subCount;
        private string val;

        [FindsBy(How = How.Id, Using = "jp-key-words")]
        IWebElement searchTextbox { get; set; }

        [FindsBy(How = How.Id, Using = "jp-search-job")]
        IWebElement findJob { get; set; }

        [FindsBy(How = How.Id, Using = "btn_search_job")]
        IWebElement UpdateButton { get; set; }

        [FindsBy(How = How.Id, Using = "add-new")]
        IWebElement addButton { get; set; }

        [FindsBy(How = How.Id, Using = "classificationAutoComplete")]
        IWebElement inputClassification { get; set; }

        public void insertText(string value)
        {
            searchTextbox.SendKeys(value);
        }

        public void selectCity(string city)
        {
            int counter=238;
            string number;
            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            // input[name = 'username']
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#GenderCheckbox']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();

            /*   if ((city!="NA"))
               {
                   Thread.Sleep(9000);
                  DriverContext.Driver.FindElement(By.XPath("//div[@id='mySidenav']/div[5]")).Click();


                   IList<IWebElement> ele = DriverContext.Driver.
                                           FindElement(By.Id("LocationCheckbox")).
                                           FindElements(By.TagName("div"));

                   for(int i = 0; i < ele.Count; i++)
                   {

                       if (i < ele.Count)
                           {
                               string s = ele[i].FindElement(By.TagName("span")).Text;
                               if (ele[i].FindElement(By.TagName("span")).Text.Contains(city))
                               {
                                   counter = counter + 1;     
                                   number = ele[i].FindElement(By.TagName("span")).Text;
                                   js.ExecuteScript("document.getElementById('checkbox_" + counter + "'" + ").click();");
                                   Thread.Sleep(9000);
                                   break;
                               }
                           }
                           else if (i == ele.Count)
                           {
                                DriverContext.Driver.FindElement(By.Id("locationAutoComplete")).Click();
                               break;
                           }
                       }

                   }*/
        }

        public void selectCityAutoSuggest(string city)
        {

        }

        public void selectGender(string gender)
        {
            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            // input[name = 'username']
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#GenderCheckbox']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();
            Thread.Sleep(5000);

            if (gender != "NA")
            {
                IList<IWebElement> ele = DriverContext.Driver.
                                        FindElement(By.Id("GenderCheckbox")).
                                            FindElements(By.TagName("div"));
                for (int i = 0; i < ele.Count; i++)
                {
                    string s = ele[i].FindElement(By.TagName("span")).Text;
                    if (ele[i].FindElement(By.TagName("span")).Text.Contains(gender))
                    {
                        IWebElement f =ele[i].FindElement(By.TagName("input"));
                        js.ExecuteScript("arguments[0].click();", f);

                        break;

                    }
                }

            }
        }

        public void selectAge(string age)
        {
            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            // input[name = 'username']
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#AgeCheckbox']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();
            Thread.Sleep(5000);

            if (age != "NA")
            {
                IList<IWebElement> ele = DriverContext.Driver.
                                        FindElement(By.Id("AgeCheckbox")).
                                            FindElements(By.TagName("div"));
                for (int i = 0; i < ele.Count; i++)
                {
                    string s = ele[i].FindElement(By.TagName("span")).Text;
                    if (ele[i].FindElement(By.TagName("span")).Text.Contains(age))
                    {
                        IWebElement f = ele[i].FindElement(By.TagName("input"));
                        js.ExecuteScript("arguments[0].click();", f);
                        break;

                    }
                }

            }

        }


        public void selectMinimumEducation(string education)
        {

            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            // input[name = 'username']
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#EducationCheckbox']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();
            Thread.Sleep(5000);

            if (education != "NA")
            {
                IList<IWebElement> ele = DriverContext.Driver.
                                        FindElement(By.Id("EducationCheckbox")).
                                            FindElements(By.TagName("div"));
                for (int i = 0; i < ele.Count; i++)
                {
                    string s = ele[i].FindElement(By.TagName("span")).Text;
                    if (ele[i].FindElement(By.TagName("span")).Text.Contains(education))
                    {
                        IWebElement f = ele[i].FindElement(By.TagName("input"));
                        js.ExecuteScript("arguments[0].click();", f);
                        break;

                    }
                }

            }

        }   

        public void selectExperiance(string exp)
        {

            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#ExperienceCheckbox']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();
            Thread.Sleep(5000);

            if (exp != "NA")
            {
                IList<IWebElement> ele = DriverContext.Driver.
                                            FindElement(By.Id("ExperienceCheckbox")).
                                            FindElements(By.TagName("div"));
                for (int i = 0; i < ele.Count; i++)
                {
                    string s = ele[i].FindElement(By.TagName("span")).Text;
                    if (ele[i].FindElement(By.TagName("span")).Text.Contains(exp))
                    {
                        IWebElement f = ele[i].FindElement(By.TagName("input"));
                        js.ExecuteScript("arguments[0].click();", f);
                        break;
                    }
                }
            }

        }

        public void selectDateListed(string date)
        {

            //
            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#DateListedCheckbox']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();
            Thread.Sleep(5000);

            if (date != "NA")
            {
                IList<IWebElement> ele = DriverContext.Driver.
                                            FindElement(By.Id("DateListedCheckbox")).
                                            FindElements(By.TagName("div"));
                for (int i = 0; i < ele.Count; i++)
                {
                    string s = ele[i].FindElement(By.TagName("span")).Text;
                    if (ele[i].FindElement(By.TagName("span")).Text.Contains(date))
                    {
                        IWebElement f = ele[i].FindElement(By.TagName("input"));
                        js.ExecuteScript("arguments[0].click();", f);
                        break;
                    }
                }
            }
        }

        public void selectWorkType(string work)
        {
            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#WorktypeCheckbox']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();
            Thread.Sleep(5000);

            if (work != "NA")
            {
                IList<IWebElement> ele = DriverContext.Driver.
                                            FindElement(By.Id("WorktypeCheckbox")).
                                            FindElements(By.TagName("div"));
                for (int i = 0; i < ele.Count; i++)
                {
                    string s = ele[i].FindElement(By.TagName("span")).Text;
                    if (ele[i].FindElement(By.TagName("span")).Text.Contains(work))
                    {
                        IWebElement f = ele[i].FindElement(By.TagName("input"));
                        js.ExecuteScript("arguments[0].click();", f);
                        break;
                    }
                }
            }
        }

        public void selectSalary()
        {
            Thread.Sleep(10000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IWebElement element = DriverContext.Driver.
                             FindElement
                             (By.CssSelector("div[data-target='#demo9']"));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            element.Click();


            IList<IWebElement> ele = DriverContext.Driver.
                                 FindElements(By.ClassName("ui-slider-handle"));

            Thread.Sleep(10000);
            new Actions(DriverContext.Driver).
                    DragAndDropToOffset(ele[0], 50,0).Build().
                        Perform();


        }

        public void selectIndustry(string industry)
        {
            if (!String.IsNullOrEmpty(industry))
            {
                {
                    new SelectElement(DriverContext.Driver.
                                      FindElement(By.Id("jp-job-cat-DD"))).
                                      SelectByText(industry);
                }
            }
        }

        public void clickFindJob()
        {
            findJob.Click();
        }

        public Boolean verifyResults()
        {
            IList<IWebElement> ele = DriverContext.Driver.FindElement(By.Id("resultant")).
                                    FindElements(By.TagName("a"));
            if (ele.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void clickOnJob()
        {
            IList<IWebElement> ele = DriverContext.Driver.FindElement(By.Id("resultant")).
                                    FindElements(By.TagName("a"));
            ele[0].Click();
            Thread.Sleep(500);
            IWebElement element = DriverContext.Driver.FindElement(By.Id("btn_post_job_by_profile_down"));
            ((IJavaScriptExecutor)DriverContext.Driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

        public Boolean IsNoSearchFound()
        {

            if (DriverContext.Driver.FindElements(By.ClassName("img-loader")).Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string checkSubClassification(string value)
        {
            string val = null;
            IList<IWebElement> ele = DriverContext.Driver.FindElement
                                  (By.Id("ClassificationCheckBox")).
                                 FindElement(By.Id("SubClassificationFetch")).
                                 FindElements(By.TagName("div"));

            for (int i = 0; i < ele.Count; i++)
            {
                if (ele[i].FindElement(By.TagName("span")).Text.Contains(value))
                {
                    val = ele[i].FindElement(By.TagName("span")).ToString();
                    ele[i].FindElement(By.TagName("input")).Click();
                    break;
                }
                else
                {
                    val = "not found";
                }
            }
            return val;
        }

        public string clickMainClassification(string value, string v2)
        {
            int counter;
            string number = null;
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IList<IWebElement> ele = DriverContext.Driver.
                                        FindElement(By.Id("ClassificationCheckBox")).
                                        FindElements(By.Id("ClassificationFetch"));
            for (int i = 0; i < ele.Count; i++)
            {
                if (ele[i].FindElement(By.TagName("span")).Text.Contains(value))
                {
                     counter = i + 3;
                    number = ele[i].FindElement(By.TagName("span")).Text;
                    js.ExecuteScript("document.getElementById('checkbox_" + counter + "'" + ").click();");
                    Thread.Sleep(9000);
                    number = selectSubClassification(v2, counter);
                    break;
                }
                else
                {

                    Thread.Sleep(9000);
                    addNewClassifiacion(value, v2);
                    break;
                }
            }
            return number;
        }

        public int clickMainClassificationNew(string value, string v2)
        {
            int counter;
            int sum=0;
            string number = null;
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IList<IWebElement> ele = DriverContext.Driver.
                                        FindElement(By.Id("ClassificationCheckBox")).
                                        FindElements(By.Id("ClassificationFetch"));
            for (int i = 0; i < ele.Count; i++)
            {
                if (ele[i].FindElement(By.TagName("span")).Text.Contains(value))
                {
                    counter = i + 3;
                    number = ele[i].FindElement(By.TagName("span")).Text;
                    js.ExecuteScript("document.getElementById('checkbox_" + counter + "'" + ").click();");
                    Thread.Sleep(9000);
                    sum= newSelectSubClassification(v2);
                    break;
                }
                else
                {

                    Thread.Sleep(9000);
                    addNewClassifiacion(value, v2);
                    break;
                }
            }
            return sum;
        }

        public int clickMainMultipleClassificationNew(string value, string v2)
        {
            int j = 0;
            int counter;
            int sum = 0;
            string number = null;
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IList<IWebElement> ele = DriverContext.Driver.
                                        FindElement(By.Id("ClassificationCheckBox")).
                                        FindElements(By.Id("ClassificationFetch"));

            string [] parts = value.Split(';');

            
            for (int i = 0; i <= ele.Count; i++)
            {
                if (i < ele.Count)
                {
                    if (ele[i].FindElement(By.TagName("span")).Text.Trim().StartsWith(value.Trim()))
                    {
                        counter = i + 3;
                        number = ele[i].FindElement(By.TagName("span")).Text;
                        js.ExecuteScript("document.getElementById('checkbox_" + counter + "'" + ").click();");
                        Thread.Sleep(9000);
                        sum = newSelectSubClassification(v2);
                        break;
                    }
                }else if(i == ele.Count)
                {
                    Thread.Sleep(9000);
                    addNewClassifiacion(value, v2);
                    Thread.Sleep(9000);
                    sum = newSelectSubClassification(v2);
                    break;
                }
            }

            return sum;
        }

        public void clickAdd()
        {
            ((IJavaScriptExecutor)DriverContext.Driver).ExecuteScript("arguments[0].scrollIntoView(true);",addButton);
            Thread.Sleep(500);
            addButton.Click();
        }

        public void clickAutoSuggestClassification(String value)
        {
            if (DriverContext.Driver.FindElements(By.Id("ui-id-1")).Count > 0)
            {
                IList<IWebElement> ele = DriverContext.Driver.
                                            FindElement(By.Id("ui-id-1"))
                                            .FindElements(By.TagName("li"));

                for (int i = 0; i < ele.Count; i++)
                {
                    if (ele[i].Text.Contains(value))
                    {
                        ele[i].Click();
                        Thread.Sleep(10000);
                        inputClassification.SendKeys(Keys.Enter);
                        break;
                    }
                }

            }
        }

        public void insertAutoSubClassification(String v2)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)DriverContext.Driver;
            string number;
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IList<IWebElement> ele = DriverContext.Driver.
                                    FindElement(By.Id("ClassificationCheckBox")).
                                    FindElements(By.Id("SubClassificationFetch"));

            for (int i = 0; i < ele.Count; i++)
            {
                string s = ele[i].FindElement(By.TagName("span")).Text.ToString();
                if (ele[i].Text.Contains(v2))
                {
                    int size = ele[i].FindElements(By.TagName("div")).Count();

                    IList<IWebElement> fa= ele[i].FindElements(By.TagName("div"));


                    for (int f = 0; f <size; f++)
                    {
                        if (fa[f].FindElement(By.TagName("span")).Text.Contains(v2))
                        {
                            IWebElement element = fa[f].FindElement(By.TagName("input"));
                            executor.ExecuteScript("arguments[0].click();", element);   
                            break;
                        }
                    }
                  
                }
            }

        }

        public void addNewClassifiacion(string value, string v2)
        {

            Thread.Sleep(9000);
            clickAdd();
            Thread.Sleep(9000);
            inputClassification.SendKeys(value);
            Thread.Sleep(9000);
            clickAutoSuggestClassification(value);
            Thread.Sleep(9000);
        }

        public string selectSubClassification(string value, int counter)
        {
            string number = null;
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            IWebElement ele = DriverContext.Driver.FindElement(By.
                                Id("SubClassificationCheckBox_" + counter + ""));

            IList<IWebElement> el = ele.FindElement(By.Id("SubClassificationFetch")).
                                    FindElements(By.TagName("div"));

            for (int i = 0; i < el.Count; i++)
            {
                if (el[i].FindElement(By.TagName("span")).Text.Contains(value))
                {
                    number = el[i].FindElement(By.TagName("span")).Text;
                    IWebElement eli = el[i].FindElement(By.TagName("input"));
                    js.ExecuteScript("arguments[0].scrollIntoView(true);", eli);
                    Thread.Sleep(500);
                    Thread.Sleep(10000);
                    js.ExecuteScript("arguments[0].click();", eli);
                    break;
                }
            }

            return number;
        }

        public int newSelectSubClassification(string value)
        {
            int sum = 0;
            int total=0;
            string number = null;
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            string[] parts = value.Split(';');
            IList<IWebElement> ele;
            for (int j = 0; j < parts.Count(); j++)
            {        
                ele = DriverContext.Driver.
                             FindElement(By.Id("ClassificationCheckBox")).
                             FindElements(By.Id("SubClassificationFetch"));

                for (int i = 0; i < ele.Count; i++)
                {
                    string s = ele[i].FindElement(By.TagName("span")).Text.ToString();
                    if (ele[i].Text.Contains(parts[j]))
                    {
                        int size = ele[i].FindElements(By.TagName("div")).Count();
                    
                        IList<IWebElement> fa = ele[i].FindElements(By.TagName("div"));
                        for (int f = 0; f < fa.Count; f++)
                        {
                            if (fa[f].FindElement(By.TagName("span")).Text.Contains(parts[j]))
                            {
                                string test = (Regex.Replace(fa[f].FindElement(By.TagName("span")).Text, @"[.\D+]", ""));
                                Console.WriteLine(test);
                                if (!string.IsNullOrWhiteSpace(fa[f].FindElement(By.TagName("span")).Text))
                                {
                                   
                                    IWebElement element = fa[f].FindElement(By.TagName("input"));
                                    if (!element.Selected)
                                    {
                                        js.ExecuteScript("arguments[0].click();", element);
                                        if (!string.IsNullOrWhiteSpace(test))
                                        {
                                            sum = +Convert.ToInt32(test);
                                            total += sum;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                    }
                }
            }

            return total;          
        }

        public String connectStoreProc(string classification, string subclass, string location, string gender, string age, string education, string experiance, string listed, string worktype)
        {
            //XEC[dbo].[GetJobsCountbyFilter] 'AEB271EF-F11F-4904-BD76-7970CB6F2583','NA','NA','Developers And Programmers','NA','NA','NA','NA','NA','NA','NA', 0, 0, 10, 1, 'Jobs,GraduateJobs,VolunteeringJobs' ,'00000000-0000-0000-0000-000000000000'

            string connetionString = null;
            SqlConnection connection;
            SqlCommand command;
            string sql = null;
            SqlDataReader dataReader;
            connetionString = "Data Source=KBSPAKLHRSVR002;Initial Catalog=Jsp_Stagging_Db;User ID=obakhtiar;Password=omer123";

            sql = "EXEC[dbo].[GetJobsCountbyFilter] 'AEB271EF-F11F-4904-BD76-7970CB6F2583','NA','" + classification + "'" + ",'" + subclass + "'" + ",'" + listed + "'" + ",'" + worktype + "'" + ",'" + location + "'" + ",'" + gender + "'" + ",'" + age + "'" + ",'" + education + "'" + ",'" + "NA" + "'" + ", 0, 0, 10, 1, 'Jobs,GraduateJobs,VolunteeringJobs' ,'00000000-0000-0000-0000-000000000000';";
            connection = new SqlConnection(connetionString);
            try
            {
                connection.Open();
                command = new SqlCommand(sql, connection);
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    val = (dataReader["TotalJobs"].ToString());

                }
                dataReader.Close();
                command.Dispose();
                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(" i got exception");
            }

            return val;

        }

        public void clickUpdateButton()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            js.ExecuteScript("arguments[0].scrollIntoView(true);", UpdateButton);
            Thread.Sleep(5000);
            UpdateButton.Click();
        }

        public int selectLocation(string value)
        {
            int num;
            bool success = int.TryParse(new string(value
                    .SkipWhile(x => !char.IsDigit(x))
                    .TakeWhile(x => char.IsDigit(x))
                    .ToArray()), out num);

            return num;
        }

        public Boolean checkNotFound()
        {
            if (DriverContext.Driver.FindElements(By.Id("jobnotfound")).Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



    }
}
