﻿using System;
using EAAutoFramework.Base;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Data.SqlClient;

namespace EAEmployeeTest.Pages
{
    internal class CreateEmployeePage : BasePage
    {

        [FindsBy(How = How.Id, Using = "il-emp-fn")]
        IWebElement firstName { get; set; }

        [FindsBy(How = How.Id, Using = "il-emp-ln")]
        IWebElement lastName { get; set; }

        [FindsBy(How = How.Id, Using = "il-emp-ph")]
        IWebElement mobileNumber { get; set; }

        [FindsBy(How = How.Id, Using = "il-emp-email")]
        IWebElement  email { get; set; }

        [FindsBy(How = How.Id, Using = "il-emp-pwd")]
        IWebElement  password { get; set; }

        [FindsBy(How = How.Id, Using = "il-emp-conf-pwd")]
        IWebElement confirmPassword { get; set; }

        [FindsBy(How = How.TagName, Using = "title")]
        IWebElement title;

        [FindsBy(How = How.Id, Using = "submit-jobseeker")]
        IWebElement submit;

        [FindsBy(How = How.ClassName, Using = "careerz_css-checkbox")]
        IWebElement accept;

        [FindsBy(How = How.Id, Using = "seeker-Err-Msg")]
        public  IWebElement errormessage ;

        [FindsBy(How = How.Id, Using = "il-verf-code-in")]
        public IWebElement code;

        [FindsBy(How = How.Id, Using = "il-sbmt-btn")]
        public IWebElement button;

        //il-sbmt-btn
        public static String val;

        public  void CreateEmployee(string first,string last,string num,string eml,string pass,string conpass,string clas,string sub) {
            Thread.Sleep(5000);
            firstName.SendKeys(first);
            lastName.SendKeys(last);
            mobileNumber.SendKeys(num.ToString());
            email.SendKeys(eml);
            password.SendKeys(pass);
            confirmPassword.SendKeys(conpass);
            Thread.Sleep(5000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            js.ExecuteScript("javascript:window.scrollBy(250,350)");
            Thread.Sleep(5000);


            try
            {
                new SelectElement(DriverContext.Driver.FindElement(By.
                             Id("il-emp-stdplstdy-type-DD"))).
                             SelectByText(clas);
                Thread.Sleep(9000);
                new SelectElement(DriverContext.Driver.FindElement(By.
                                  Id("il-emp-stdplstdy-subClassification"))).
                                  SelectByText(sub);
            }
            catch (NoSuchElementException e)
            {
                Console.Out.WriteLine("Element absent");
            }
        }

        public void AcceptAggrement()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            js.ExecuteScript("$('#TAC').click();");
        }

        public void SubmitForm()
        {
            Thread.Sleep(5000);
            submit.Click();
        }

        public Boolean IsPresent()
        {

            string s = DriverContext.Driver.Title;
            if (DriverContext.Driver.Title.Contains("Registration"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean message(string message)
        {
            if (errormessage.Text.Contains(message))
            {
                return true;
            }else
            {
                return false;
            }
        }

        public String connectToDB()
        {
            string connetionString = null;
            SqlConnection connection;
            SqlCommand command;
            string sql = null;
            SqlDataReader dataReader;
            connetionString = "Data Source=KBSPAKLHRSVR002;Initial Catalog=Jsp_Stagging_Db;User ID=obakhtiar;Password=omer123";

            sql = "Select top 1 VerificationCode from [User] order By DateCreated desc;";
            connection = new SqlConnection(connetionString);
            try
            {
                connection.Open();
                command = new SqlCommand(sql, connection);
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    val = (dataReader["VerificationCode"].ToString());

                }
                dataReader.Close();
                command.Dispose();
                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(" i got exception");
            }

            return TruncateLongString(val);

        }

   


        public string TruncateLongString(string str)
        {
            return str.Substring(0, Math.Min(str.Length, 5));
        }

        public void verifyCode(string s)
        {
            code.SendKeys(s);
        }

        public void submitCode()
        {
            submit.Click();
        }




    }
}
