﻿using EAAutoFramework.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAEmployeeTest.Pages
{
    public class LahoreData:BasePage
    {
        [FindsBy(How = How.Id, Using = "btnadv_search")]
        IWebElement advanceSearch { get; set; }

        [FindsBy(How = How.Id, Using = "busines_type")]
        IWebElement businessType { get; set; }

        [FindsBy(How = How.Id, Using = "sector")]
        IWebElement businessSector { get; set; }

        [FindsBy(How = How.Id, Using = "btnfind2")]
        IWebElement button { get; set; }

        public void clickOnAdvance()
        {
            advanceSearch.Click();
        }

        public void selectBuisnessType()
        {

            ((IJavaScriptExecutor)DriverContext.Driver).ExecuteScript("$('#busines_type').val('2');");
        }

        public void selectBuisnessSector(string sector)
        {
            ((IJavaScriptExecutor)DriverContext.Driver).ExecuteScript("$('#sector').val('2');");
        }


        public void clickButton()
        {
            button.Click();
        }


       
    }
}
