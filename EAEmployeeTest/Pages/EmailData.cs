﻿using EAAutoFramework.Base;
using EAEmployeeTest.Steps;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EAEmployeeTest.Pages
{
    public class EmailData : BasePage {

        [FindsBy(How = How.Id, Using = "dvMajorCity")]
        IWebElement cityName { get; set; }
    
        ExcelFile excel = new Steps.ExcelFile();
        public void getCityName(string name)
        {
            IList<IWebElement> list = cityName.FindElement(By.TagName("ul"))
                                        .FindElements(By.TagName("li"));

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].FindElement(By.TagName("a")).Text.Contains(name))
                {
                    list[i].FindElement(By.TagName("a")).Click();
                    break;
                }
            }
        }



        public void getCat(string category)
        {
            IList<IWebElement> il = DriverContext.Driver.FindElement
                                    (By.ClassName("mylist")).
                                            FindElement(By.TagName("ul")).
                                            FindElements(By.TagName("li"));
            excel.openExcel();
            for (int i = 0; i < il.Count; i++)
            {
                if (il[i].FindElement(By.TagName("a")).Text.Contains(category))
                {
                    il[i].FindElement(By.TagName("a")).Click();
                    break;
                }
            }
        }

        public void getSub(string sub) { 
            Thread.Sleep(5000);

            IList<IWebElement> elas = DriverContext.Driver.FindElement(By.Id("dvBussiness")).FindElements(By.ClassName("row"));
            IList<IWebElement> elef = elas[0].
                         FindElement(By.ClassName("mylist")).FindElement(By.TagName("ul")).FindElements(By.TagName("li"));
            for (int t = 0; t< elef.Count; t++)
            {
                if (elef[t].FindElement(By.TagName("a")).Text.Contains(sub))
                {
                    elef[t].FindElement(By.TagName("a")).Click();
                    break;
                }
            }
       
     
         
            int count = CountPage();
            int counter = 0;
            int j = 1;
            for (int k = 0; k <= count; k++)
            {
               
                IList<IWebElement> ela = DriverContext.Driver.FindElement(By.Id("dvBussiness")).FindElements(By.ClassName("row"));
                IList<IWebElement> ele = ela[1].
                             FindElements(By.CssSelector("div[id^='Bus'"));
                for (int i = 0; i < ele.Count; i++)
                {
                    saveEachRecord(ele, i, j);
                    j++;
                }
       
                Thread.Sleep(10000);
                if (k == 0)
                {
                    k=k + 1;
                }
                DriverContext.Driver.Navigate().GoToUrl("http://businessdirectory.pk/Default.aspx?action=Business&pid=307880&cid=630595&cityid=279285&page=" + k);
                Thread.Sleep(10000);
            }
         
            excel.closeExcel();
       
        }

        public void saveEachRecord(IList<IWebElement> ele, int index, int j)
        {

            string[] split = ele[index].Text.Split('\n');
            if (split.Length >= 4)
            {
                string companyName = split[0];
                string address = split[2];
                string phone = split[3];
                string email = "not available";
                if (split.Length >= 5)
                {
                    email = split[4];
                }
                int c = 0;
                int a = 0;
                
                excel.addDataToExcel(companyName, address, phone, email, j);
            }
        }

        public int CountPage()
        {
            
            if (DriverContext.Driver.FindElements(By.Id("ctl00_cntMainCenter_ctl00_lblPageLinks")).Count >  0  )
            {
                IList<IWebElement> ele = DriverContext.Driver.FindElement
                                            (By.Id("ctl00_cntMainCenter_ctl00_lblPageLinks")).
                                              FindElements(By.TagName("a"));
                return ele.Count;

            }else{
                return 0;
            }
        }

    }

}

