﻿using EAAutoFramework.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EAEmployeeTest.Pages
{
    public class Employers : BasePage
    {
        [FindsBy(How = How.ClassName, Using = "tab-employer")]
        IWebElement emp { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-fn")]
        IWebElement firstName { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-ln")]
        IWebElement lastName { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-email")]
        IWebElement email { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-cnic")]
        IWebElement cnic { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-pwd")]
        IWebElement password { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-conf-pwd")]
        IWebElement confirmPassword { get; set; }

        [FindsBy(How = How.ClassName, Using = "btn-register-green")]
        IWebElement nextButton { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-bn")]
        IWebElement buisnessName { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-bus-url")]
        IWebElement buisnesWeb { get; set; }

        [FindsBy(How = How.Id, Using = "il-empr-ph")]
        IWebElement mobilePhone { get; set; }

        [FindsBy(How = How.Id, Using = "Empr-step1-Err-Msg")]
        IWebElement errormessage { get; set; }

        [FindsBy(How = How.ClassName, Using = "reg-btn-next")]
        IWebElement secondNext { get; set; }

        [FindsBy(How = How.ClassName, Using = "reg-btn-prev")]
        IWebElement previousButton { get; set; }

        [FindsBy(How = How.Id, Using = "Empr-step2-Err-Msg")]
        IWebElement errormessageTwo { get; set; }

        [FindsBy(How = How.Id, Using = "il-locsrc-as-add")]
        IWebElement enterLocation { get; set; }

        [FindsBy(How = How.Id, Using = "il-stad1-input")]
        IWebElement addressOne { get; set; }

        [FindsBy(How = How.Id, Using = "il-stad2-input")]
        IWebElement addressTwo { get; set; }

        [FindsBy(How = How.Id, Using = "il-pc-input")]
        IWebElement postCode { get; set; }
        //il-stad1-input

        [FindsBy(How = How.Id, Using = "TAC-Empr")]
        IWebElement acceptCode { get; set; }

        [FindsBy(How = How.Id, Using = "submit-emp-register")]
        IWebElement submitForm { get; set; }

        [FindsBy(How = How.Id, Using = "il-sbmt-btn")]
        IWebElement submitCode { get; set; }

        //submit-emp-register

        //il-sbmt-btn

        public void clickEmp()
        {
            emp.Click();
        }

        public void employerData(string p0, string p1, string p2, string p3, string p4, string p5)
        {
            firstName.SendKeys(p0);
            lastName.SendKeys(p1);
            email.SendKeys(p2);
            cnic.SendKeys(p3);
            password.SendKeys(p4);
            confirmPassword.SendKeys(p5);
        }

        public void selectBuisnessType(string type) {
            try
            {
                new SelectElement(DriverContext.Driver.FindElement(By.
                                Id("il-business-type-DD"))).
                                SelectByText(type);
            }
            catch (NoSuchElementException e)
            {
                Console.Out.WriteLine("Element absent");
            }

        }
        public void selectMainClassifications(string clas)
        {

            try
            {
                new SelectElement(DriverContext.Driver.FindElement(By.
                             Id("il-industral-Cat-DD"))).
                             SelectByText(clas);
            }
            catch (NoSuchElementException e)
            {
                Console.Out.WriteLine("Element absent");
            }
        }

        public void clickFirstNext()
        {
            var element = DriverContext.Driver.FindElement(By.ClassName("btn-register-green"));
            Actions actions = new Actions(DriverContext.Driver);
            actions.MoveToElement(element);
            actions.Perform();
            Thread.Sleep(500);
            nextButton.Click();
        }

        public Boolean invalidMessage(string message)
        {
            if (errormessage.Text.Contains(message))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean invalidSecondMessage(string message)
        {
            if (errormessageTwo.Text.Contains(message))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean isNotDisplayed()
        {
            if (buisnessName.Displayed)
            {
                return true;
            } else
            {
                return false;
            }
        }

        public void insertSecondFormData(string p0, string p1, string p3, string p4, string p5)
        {
            buisnessName.SendKeys(p0);
            selectBuisnessType(p1);
            buisnesWeb.SendKeys(p3);
            mobilePhone.SendKeys(p4);
            selectMainClassifications(p5);
        }

        public void nextButtonOfForm()
        {
            secondNext.Click();
        }

        public void previousClick()
        {
            previousButton.Click();


        }
        public void clearFieldsFirstForm()
        {
            firstName.Clear();
            lastName.Clear();
            email.Clear();
            cnic.Clear();
            password.Clear();
            confirmPassword.Clear();

        }
        public void selectLocation(string location)
        {
            enterLocation.SendKeys(location);
            Thread.Sleep(5000);
            IList<IWebElement> ele = DriverContext.Driver.
                                        FindElement(By.Id("ui-id-1")).
                                        FindElements(By.TagName("li"));
            Thread.Sleep(7000);
            for (int i = 0; i < ele.Count; i++)
            {
                if (ele[i].Text.Contains("KARACHI"))
                {
                    ele[i].Click();
                } else
                {
                    Console.WriteLine("not found city");
                }
            }
        }
        public Boolean stateIsSelected(string city, string state, string country)
        {
            if (checkState(state) && checkCountry(country) == true && checkCity(city) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean checkCity(string city)
        {
            Boolean a;
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            string title = (string)js.ExecuteScript("return $('.ui-menu-item:first div').text();");


            if (title.Contains(city)) {
                a = true;
            } else
            {
                a = false;
            }

            return a;
        }
        public Boolean checkState(string state)
        {
            Boolean b = false;
            IList<IWebElement> ele = DriverContext.Driver.
                                FindElement(By.Id("il-region-DD")).
                                FindElements(By.TagName("option"));
            for (int i = 0; i < ele.Count; i++)
            {
                if (ele[i].Text.Equals(state))
                {
                    return b = true;
                    break;
                }
                else
                {
                    b = false;

                }

            }

            return b;
        }
        public Boolean checkCountry(string country)
        {
            Boolean a;
            IList<IWebElement> ele = DriverContext.Driver.
                                FindElement(By.Id("il-countri-DD")).
                                FindElements(By.TagName("option"));

            if (ele[1].Text.Equals(country))
            {
                a = true;
            }
            else
            {
                a = false;
            }

            return a;
        }

        public void insertAddressOne(string addr)
        {
            addressOne.SendKeys(addr);
        }


        public void insertAddressTwo(string addr)
        {
            addressTwo.SendKeys(addr);
        }

        public void insertZipCode(int zip)
        {
            postCode.SendKeys(zip.ToString());
        }

        public void acceptAgreement()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            js.ExecuteScript("$('#TAC-Empr').click();");
        }

        public void clearAdress()
        {
            addressOne.Clear();
            addressTwo.Clear();
            postCode.Clear();
        }

        public void clickSubmit()
        {

            submitForm.Click();

        }

        public void submitCd()
        {
            Thread.Sleep(500);
            submitCode.Click();
        }

    }
}
