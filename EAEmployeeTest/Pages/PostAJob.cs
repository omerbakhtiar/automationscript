﻿using EAAutoFramework.Base;
using EAAutoFramework.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EAEmployeeTest.Pages
{
    public class PostAJob:BasePage
    {
        [FindsBy(How = How.LinkText, Using = "Post A Job")]
        IWebElement postJob { get; set; }

        [FindsBy(How = How.Id, Using = "confirmationBtn")]
        IWebElement continueButton { get; set; }

        [FindsBy(How = How.Id, Using = "SearchResultTitle")]
        IWebElement searchTitle { get; set; }

        [FindsBy(How = How.Id, Using = "JobTitle")]
        IWebElement jobTitle { get; set; }


        [FindsBy(How = How.Id, Using = "ShortDescription")]
        IWebElement shortDescription { get; set; }

        [FindsBy(How = How.Id, Using = "JobDetailsTextArea")]
        IWebElement jobDetail { get; set; }

        [FindsBy(How = How.Id, Using = "Video")]
        IWebElement video { get; set; }

        [FindsBy(How = How.Id, Using = "NoOfPosts")]
        IWebElement NoPosts { get; set; }

        [FindsBy(How = How.Id, Using = "VisibleSalary")]
        IWebElement visibleSalary { get; set; }

        [FindsBy(How = How.Id, Using = "PreviewJob")]
        IWebElement contin  { get; set; }

        public void clickPostJob()
        {
            postJob.Click();
            Thread.Sleep(10000);
        }

        public void clickContinue()
        {
            Thread.Sleep(9000);
            continueButton.Click();
        }

        public void selectJobType(string value)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            if (value == "Jobs")
            {
                js.ExecuteScript("$('#jobBtn').click();");

            }
            else if (value == "GraduateJobs&Internships")
            {
                
                js.ExecuteScript("$('#graduateBtn').click();");
            }
            else if (value == "Volunteer")
            {
                js.ExecuteScript("$('#volunteerBtn').click();");
            }
        }
       
        public void selectJobEdition(string value)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            if (value == "Standard")
            {
                js.ExecuteScript("$('#normalPostBtn').click();");

            }
            else if (value == "Premium")
            {
                js.ExecuteScript("$('#premiumPostBtn').click();");

            }
        }

        public void insertSearchTitle()
        {
            searchTitle.SendKeys(ExcelHelpers.ReadData(1, "SearchResultsTitle").ToString());
        }

        public void insertJobTitle()
        {
            jobTitle.SendKeys(ExcelHelpers.ReadData(1,"JobTitle").ToString());
        }

        public void insertShortDescripton()
        {
            insertSearchTitle();
            insertJobTitle();
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            string s=(ExcelHelpers.ReadData(1,"Short").ToString());
            s = s.Replace("  ", string.Empty).Replace("'", string.Empty).Replace("\n", " ");
            string jsFunc = "$('#ShortDescription').trumbowyg('html','" + s + "');";
            js.ExecuteScript(jsFunc);
            insertJobDetail();
        }

        public void insertJobDetail()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            string s = ExcelHelpers.ReadData(1, "JobDetails");
            s = s.Replace("  ", string.Empty).Replace("'", string.Empty).Replace("\n", " ");
            string jsFunc = "$('#JobDetailsTextArea').trumbowyg('html','"+s+"');";
            js.ExecuteScript(jsFunc);
            Thread.Sleep(10000);
            js.ExecuteScript("Job.GetSkill();");
            Thread.Sleep(5000);
            insertContact();
            Thread.Sleep(5000);
            selectLocation();
            selectClassification();
            Thread.Sleep(5000);
            selectSubClassification();
            selectWorkType();
            selectPayStucture();
            selectNoOfPosts();
            selectMinLimit();
            selectMaxLimit();
            insertSalary();
            selectGender();
            selectMinAge();
            selectMaxAge();
            selectEducation();
            selectDegreeTitle();
            selectMinYearsOfExperience();
            selectMaxYearsOfExperience();
            selectClosingDate();
            Thread.Sleep(5000);
            contin.Click();
            Thread.Sleep(5000);
            clickNoAttachTest();
            Thread.Sleep(5000);
            clickPost();

        }

        public void insertContact()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)DriverContext.Driver;
            string s = ExcelHelpers.ReadData(1, "ContactDetails");
            string jsFunc = "$('#ContactDetails').trumbowyg('html','" + s + "');";
            js.ExecuteScript(jsFunc);
        }

        public void selectLocation()
        {
            IWebElement element = DriverContext.Driver.FindElement(By.Id("Location1"));
            ((IJavaScriptExecutor)DriverContext.Driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            string s = ExcelHelpers.ReadData(1, "Location");
            //Location
            new SelectElement(DriverContext.Driver.
                                FindElement(By.Id("Location1"))).
                                SelectByText(s);
        }
        public void selectClassification()
        {
            string s= ExcelHelpers.ReadData(1, "MainClassification");
            new SelectElement(DriverContext.Driver.
                               FindElement(By.Id("MainClassification"))).
                               SelectByText(s);
        }

        public void selectSubClassification()
        {
            string s = ExcelHelpers.ReadData(1,"SubClassification");
            new SelectElement(DriverContext.Driver.
                               FindElement(By.Id("SubClassification"))).
                               SelectByText(s);
        }

        public void selectWorkType()
        {
            string s = ExcelHelpers.ReadData(1, "WorkType");
            new SelectElement(DriverContext.Driver.
                               FindElement(By.Id("WorkType"))).
                               SelectByText(s);         
        }

        public void selectPayStucture()
        {
            string s = ExcelHelpers.ReadData(1, "PayStructure");
            new SelectElement(DriverContext.Driver.
                               FindElement(By.Id("PayStructure"))).
                               SelectByText(s);      
        }

        public void selectNoOfPosts()
        {
            string s = ExcelHelpers.ReadData(1, "NoOfPosts");
            NoPosts.Clear();
            NoPosts.SendKeys(s);
        }

        public void selectMinLimit()
        {
            string s = ExcelHelpers.ReadData(1,"MinLimit");
            new SelectElement(DriverContext.Driver.
                               FindElement(By.Id("SalaryRange1"))).
                               SelectByText(s);
            //MinLimit
        }

        public void selectMaxLimit()
        {
            string s = ExcelHelpers.ReadData(1, "MaxLimit");
            Thread.Sleep(5000);
            new SelectElement(DriverContext.Driver.
                               FindElement(By.Id("SalaryRange2"))).
                               SelectByText(s);
        }

        public void insertSalary()
        {
            string s = ExcelHelpers.ReadData(1, "VisibleSalary");
            visibleSalary.SendKeys(s);
        }

        public void selectGender()
        {
            string s = ExcelHelpers.ReadData(1,"Gender");
            new SelectElement(DriverContext.Driver.
                             FindElement(By.Id("Gender"))).
                             SelectByText(s);
        }

        public void selectMinAge()
        {
            string s = ExcelHelpers.ReadData(1, "Age1");
            new SelectElement(DriverContext.Driver.
                             FindElement(By.Id("Age1"))).
                             SelectByText(s);        
        }

        public void selectMaxAge()
        {
            string s = ExcelHelpers.ReadData(1, "Age2");
            Thread.Sleep(500);
            new SelectElement(DriverContext.Driver.
                             FindElement(By.Id("Age2"))).
                             SelectByText(s);
        }

        public void selectEducation()
        {
            string s = ExcelHelpers.ReadData(1, "MinimumEducation");
            new SelectElement(DriverContext.Driver.
                             FindElement(By.Id("MinimumEducation"))).
                             SelectByText(s);
        }

        public void selectDegreeTitle()
        {
            string s = ExcelHelpers.ReadData(1, "DegreeTitle");
            Thread.Sleep(500);
            new SelectElement(DriverContext.Driver.
                             FindElement(By.Id("DegreeTitle"))).
                             SelectByText(s);
        }

        public void selectMinYearsOfExperience()
        {
            string s = ExcelHelpers.ReadData(1, "YearsOfExperience1");
            new SelectElement(DriverContext.Driver.
                             FindElement(By.Id("YearsOfExperience1"))).
                             SelectByText(s);
        }

        public void selectMaxYearsOfExperience()
        {
            string s = ExcelHelpers.ReadData(1, "YearsOfExperience2");
            Thread.Sleep(500);
            new SelectElement(DriverContext.Driver.
                             FindElement(By.Id("YearsOfExperience2"))).
                             SelectByText(s);
        }
        public void selectClosingDate()
        {
            string s = ExcelHelpers.ReadData(1, "ClosingDate");
            DriverContext.Driver.
                             FindElement(By.Id("datepicker")).SendKeys(s);         
        }
        public void clickNoAttachTest()
        {
            DriverContext.Driver.FindElement(By.ClassName("btn-white")).Click();
        }

        public void clickPost()
        {
            IWebElement element = DriverContext.Driver.FindElement(By.Id("SubmitPostJob"));
            ((IJavaScriptExecutor)DriverContext.Driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            element.Click();
            //SubmitPostJob
        }

    }
}
