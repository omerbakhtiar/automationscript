﻿using System;
using EAAutoFramework.Base;
using EAAutoFramework.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace EAEmployeeTest.Pages
{
    internal class HomePage : BasePage
    {

        [FindsBy(How = How.LinkText, Using = "Employee List")]
        IWebElement lnkEmployeeList { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@title='Manage']")]
        IWebElement lnkLoggedInUser { get; set; }

        [FindsBy(How = How.LinkText, Using = "Log off")]
        IWebElement lnkLogoff { get; set; }

        [FindsBy(How = How.ClassName, Using = "login")]
        IWebElement lnkLogin { get; set; }

        [FindsBy(How = How.ClassName, Using = "close-popup")]
        IWebElement popup { get; set; }

        [FindsBy(How = How.ClassName, Using = "register")]
        IWebElement register;

        internal void CheckIfLoginExist()
        {
            lnkLogin.AssertElementPresent();
        }

        public void Close()
        {
            DriverContext.Driver.Manage().Window.Maximize();
            Thread.Sleep(9000);
            popup.Click();
        }
         internal LoginPage ClickLogin()
        {
            lnkLogin.Click();
            return GetInstance<LoginPage>();
        }

        internal string GetLoggedInUser()
        {
            return lnkLoggedInUser.GetLinkText();
        }

        public void registerClick()
        {
            Thread.Sleep(4000);
            register.Click();
        }


 

    }
}
