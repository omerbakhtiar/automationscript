﻿using EAAutoFramework.Base;
using TechTalk.SpecFlow;
using EAAutoFramework.Helpers;

namespace EAEmployeeTest
{

    [Binding]
    public class HookInitialize : TestInitializeHook
    {

        public HookInitialize() : base(BrowserType.Chrome)
        {
            InitializeSettings();
            NaviateSite();
        }


        //[BeforeFeature]
        [BeforeScenario]
        public static void TestStart()
        {
            HookInitialize init = new HookInitialize();
        }

    }
}
