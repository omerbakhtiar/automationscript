﻿Feature: ExternalSearch
	Check if the External Search functionality is working
	as expected with valid data

@smoke @positive
Scenario Outline: Check External Search with valid data in scenario outline
	Given I have navigated to the application
	And I close the popup
	And I enter "<text>" in external search
	And I select "<city>" from the drop down appearing on the page
	Then i select "<industry>" from the drop down on that page
	Then I click on find job button to searcch the job
	Then I verify results are appearing as expected
	Then I click on the Job
	Examples: 
	| text | city       | industry                                  |
	| .Net |Lahore      | Information And Communication Technology  |