﻿Feature: ExternalMultiple
		Check if the External links of the employer profile on Index Page
		is working as expected of multiple selection

@multipleMainSubclassification
Scenario Outline: Check External Links of the employer is working on External multiple
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select again "<MainClassification>","<Sub-Classification>","<Location>","<Gender>","<Age>","<MinimumEducation>","<YearExperience>","<DateListed>" and "<Work Type>" and "<update>" of new checkExternal
	Examples:
            | MainClassification                                                                          | Sub-Classification                                                                                                                                                                               | Location   | Gender | Age    | MinimumEducation | YearExperience | DateListed        | Work Type        | update |
            | Information And Communication Technology;Marketing And Communications;Healthcare And Medical| Developers And Programmers;Application Support And Administration;Artificial Intelligence;Help Desk And IT Support;Networks And Systems Administration;Brand Management;Ambulance And Paramedics |LAHORE      | Male   | 21-24  | PostGraduate     | 2 Year         | Last Week         | Full Time        | test   |