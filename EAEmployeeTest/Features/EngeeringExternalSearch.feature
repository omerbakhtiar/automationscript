﻿Feature: ExternalSearchEngineering
		Check if the External links of the employer profile on Index Page
		is working as expected

@Design and Architecture
Scenario Outline: Check External Links of the employer is working on the search page using sales category DesignArchitecture
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification | Sub-Classification                    | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Engineering        | Aerospace Engineering                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Automotive Engineering                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Building Services Engineering         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Chemical Engineering                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Civil Engineering                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Electrical And Electronic Engineering | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Engineering Drafting                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Environmental Engineering             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Field Engineering                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Project Engineering                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Industrial Engineering                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Maintenance                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Engineering        | Management                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Engineering        | Process Engineering                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Engineering        | Materials Handling Engineering        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Engineering        | Mechanical Engineering                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Engineering        | Sales                                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Engineering        | Supervisors                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Engineering        | Systems Engineering                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Engineering        | Water And Waste Engineering           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		


