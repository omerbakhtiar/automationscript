﻿Feature: ExternalSearchSales
		Check if the External links of the employer profile on Index Page
		is working as expected

@sales
Scenario Outline: Check External Links of the employer is working on the search page using sales category
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification | Sub-Classification                    | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Sales              | Account And Relationship Management   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Sales              | Analysis And Reporting                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Sales              | Management                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Sales              | New Business Development              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Sales              | Sales Coordinators                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Sales              | Sales Representatives And Consultants | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
        