﻿Feature: AdvertisementArtAndMediaExternalSearch
	Check if the External links of the employer profile on Index Page
		is working as expected of AdvertisementArtAndMediaExternalSearch

@AdvertisementArtAndMedia 
Scenario Outline: Check External Links of the employer is working on the search page using AdvertisementArtAndMedia 
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and RetailExtenalSearch
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                     | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            |Advertisement, Art And Media            | Agency Account Management              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            | Art Direction                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            | Editing & Publishing                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            | Event Management                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Journalism & Writing                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Management                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Media Strategy, Planning & Buying       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Performing Arts                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Photography                             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Programming & Production                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Promotions                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Advertisement, Art And Media            |Other                                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
