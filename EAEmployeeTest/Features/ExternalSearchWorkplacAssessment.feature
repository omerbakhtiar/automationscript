﻿Feature: ExternalSearchWorkplacAssessment
		Check if the External links of the employer profile on Index Page
		is working as expected of SearchWork

@SearchWork
Scenario Outline: Check External Links of the employer is working on the search page using SearchWorkplacAssessment
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification | Sub-Classification                                        | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Agriculture        | Administration                                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Agriculture        | Agriculture And Fisheries Information Service             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Agriculture Business And Marketing Assistance Service     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Field Operations                                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Field Operations Service                                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        |  Finance                                                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Fisheries                                                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Livestock                                                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Pharmaceutical And Clinical Research                      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Planning Service                                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Agriculture        | Financial Management Service                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |


		

		

		

		