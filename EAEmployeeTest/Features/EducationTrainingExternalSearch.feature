﻿Feature: ExternalSearchEducationAndTraining
		Check if the External links of the employer profile on Index Page
		is working as expected for Education And Training 

@Design and Architecture
Scenario Outline: Check External Links of the employer is working on the search page using Education And Training 
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:  
            | MainClassification              | Sub-Classification                          | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Education And Training          | Childcare And Outside School Hours Care     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          | General Administration                      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Library Services And Information Management  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Management - Schools                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Non-Government Schools And Government Schools| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Research And Fellowships                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |School Education – Special Children          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Short Courses                                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Student Assistance                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Education And Training          |Teaching - Early Childhood                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Teaching - Primary                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Teaching - Secondary                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	      	| Education And Training          |Teaching - Tertiary                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Teaching - Vocational                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	        | Education And Training          |Teaching Aides And Special Needs             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Education And Training          |Tutoring                                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Education And Training          |Workplace Training And Assessment            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
	       