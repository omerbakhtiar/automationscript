﻿Feature: CallsCenterExternal
Check if the External links of the employer profile on Index Page
		is working as expected of Calls Center

@Calls Center
Scenario Outline: Check External Links of the employer is working on the search page using Calls Center
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                      | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Calls Center                           | Management And Support                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Calls Center                           | Customer Service - Customer Facing      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Calls Center                           | Sales - Inbound                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Calls Center                           | Sales - Outbound                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Calls Center                           | Customer Service - Customer Facing      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Calls Center                           | Supervisors And Team Leaders            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			