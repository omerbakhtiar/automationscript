﻿Feature: Testng1
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@mytag
Scenario: Add two numbers
	Given I have entered 50 into the calculator
	And I have entered 70 into the calculator
	When I press add
	Then the result should be 120 on the screen

		Examples: 
	| CityName | Category       | SubClassification			| sheetNumber |
	| Karachi  | Computers & IT | Computer Networking       | 5           |
	| Karachi  | Computers & IT | Computer Stationery Accessories| 6           |
	| Karachi  | Computers & IT | Database                  | 7           |
	| Karachi  | Computers & IT |       | 8        |
	| Karachi  | Computers & IT | E-Commerce        | 9       |
	| Karachi  | Computers & IT | E-Commerce        | 10           |
	| Karachi  | Computers & IT | E-Commerce        | 11          |
	| Karachi  | Computers & IT | E-Commerce        |12         |
	| Karachi  | Computers & IT | E-Commerce        | 13          |
	| Karachi  | Computers & IT | E-Commerce        | 14           |
	| Karachi  | Computers & IT | E-Commerce        | 15         |
	| Karachi  | Computers & IT | E-Commerce        | 16         |
	| Karachi  | Computers & IT | E-Commerce        | 17         |
	| Karachi  | Computers & IT | E-Commerce        |18          |
	| Karachi  | Computers & IT | E-Commerce        | 19         |
	| Karachi  | Computers & IT | E-Commerce        |  20          |
	| Karachi  | Computers & IT | E-Commerce        | 21          |
	| Karachi  | Computers & IT | E-Commerce        | 22          |
	| Karachi  | Computers & IT | E-Commerce        | 23          |
	| Karachi  | Computers & IT | E-Commerce        | 24           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce	    | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce        | 3           |
	| Karachi  | Computers & IT | E-Commerce	    | 3           |
	| Karachi  | Computers & IT | E-Commerce	    | 3           |