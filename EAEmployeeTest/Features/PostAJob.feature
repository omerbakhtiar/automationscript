﻿Feature: PostAJob
	Check if the post a job functionality is working
	as expected with valid data

@smoke @positive
Scenario Outline: Check Post a Job with valid data in scenario outline
	Given I have navigated to the application
	And I close the popup
	Then I click login link
	Then I get usename and password from the excel file
    Then I am on the HomePage of the employee
	Then I click on Post a job button
    Then I select "<JobType>" and "<PostingType>"
	Then I click on continue button
	Then I enter shortDescription from excelfile
	Examples: 
	 | JobType                         | PostingType |
	 |GraduateJobs&Internships         | Standard    |