﻿Feature: LegalExternalSearch
	Feature: HealthCareAndMedicalSearch
	Check if the External links of the employer profile on Index Page
		is working as expected of  Legal

@Legal
Scenario Outline: Check External Links of the employer is working on the search page using  LegalSearch
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification | Sub-Classification                      | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Legal              | Banking And Finance Law                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Construction Law                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Corporate And Commercial Law            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Courts And Legal Services               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Criminal And Civil Law                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Environment And Planning Law            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Family Law                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Generalists - In-House                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Generalists - Law Firm                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Industrial Relations And Employment Law | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Insurance And Superannuation Law        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Intellectual Property Law               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Law Clerks And Paralegals               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Legal Practice Management               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Legal Secretaries                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Litigation And Dispute Resolution       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Personal Injury Law                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Property Law                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Superannuation                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Legal              | Tax Law                                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Legal              | Workers' Compensation                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			