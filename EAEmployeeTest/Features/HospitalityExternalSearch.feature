﻿Feature: HospitalityExternalSearch
Check if the External links of the employer profile on Index Page
		is working as expected of Calls Center

@Hospitality
Scenario Outline: Check External Links of the employer is working on the search page using Calls Center
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                      | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Hospitality And Tourism                | Airlines                                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                |Beverage Staff                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                | Chefs And Cooks                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Hospitality And Tourism                | Customer Service Representative         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Hospitality And Tourism                |  Front Office And Guest Services        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Hospitality And Tourism                | Gaming                                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                |  Housekeeping                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                | Kitchen Staff                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
           	| Hospitality And Tourism                |Management                               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                |Reservations                             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                | Residents And Registrars                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                |Sales                                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
           	| Hospitality And Tourism                |Speech Therapy                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Hospitality And Tourism                |Tour Guides                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                |Travel Agents And Consultants            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Hospitality And Tourism                | Waiting Staff                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			