﻿Feature: InsuranceAndSuperannuationExterrnal
	Check if the External links of the employer profile on Index Page
		is working as expected of InsuranceAndSuperannuation

@InsuranceAndSuperannuation
Scenario Outline: Check External Links of the employer is working on the search page using Insurance And Superannuation
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                      | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Insurance And Superannuation           | Actuarial                               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Insurance And Superannuation           | Assessment                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Insurance And Superannuation           |Brokerage                                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Insurance And Superannuation           | Claims                                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Insurance And Superannuation           | Fund Administration                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Insurance And Superannuation           | Management                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Insurance And Superannuation           | Risk Consulting                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |







