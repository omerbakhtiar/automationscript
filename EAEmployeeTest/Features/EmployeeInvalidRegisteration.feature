﻿Feature: InvalidRegesteration with wrong data
	Check if the Register of Employee functionality is working
	as expected with different invalid data

@smoke @negative
Scenario Outline: Check Register of employee with  Invalid data
	Given I have navigated to the application
	And I close the popup
	Then I click register link
	Then I am on the Register Page
	Then I enter invalid data in "<FirstName>","<LastName>","<MobileNumber>","<email>","<password>","<confirmPassword>","<MainClasssification>" and "<Subclassification>"
	Then I click on accept application
	Then I submit the form
	Then I validate email error message "<message>" is displayed on employee registeration Page	
	Then I close the application
	Examples: 
	| FirstName | LastName | MobileNumber | email                | password | confirmPassword | MainClasssification | Subclassification          | message                                                                 |
	| ahmed     | ali      | 03224258209  | ahmed                | omer123  | omer123         | Accounting          | Accounts Officer And Clerk | Email address is invalid                                                |
	| ahmed     | ali      | 03224258209  | ahmed123@gmail.com   | omer123  | omer1234        | Accounting          | Accounts Officer And Clerk | Passwords should match                                                  |
	| ahmed     | ali      | iop24258209  | ahmed123@hotmail.com | omer123  | omer123         | Accounting          | Accounts Officer And Clerk | Please fill mandatory fields                                            |
	| 0322      | ali      | 03224258209  | ahmed340@gmail.com   | omer123  | omer123         | Accounting          | Accounts Officer And Clerk | Please fill mandatory fields                                            |
	| ahmed     | ali      | 03224258209  | ahmed95@gmail.com    | omer123  | omer123         |                     | Accounts Officer And Clerk | Please fill mandatory fields                                            |
	| ahmed     | ali      | 03224258209  | ahmed95@gmail.com    | omer123  | omer123         | Accounting          |                            | Please fill mandatory fields                                            |
	| ahmed     | ali      | 03224258209  | ahmed95@gmail.com    | omer123  | omer123         |                     |                            | Please fill mandatory fields                                            |
	|           |          |              |                      |          |                 |                     |                            | Please fill mandatory fields                                            |
    | ahmed     | ali      | 03224258209  | omerbk@gmail.com     | omer123  | omer123         | Accounting          | Accounts Officer And Clerk | Email is already exist                                                  |