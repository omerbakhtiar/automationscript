﻿Feature: CommunityServicesAndDevelopment
Check if the External links of the employer profile on Index Page
		is working as expected of CommunityServicesAndDevelopment

@RetailExnternalSearch
Scenario Outline: Check External Links of the employer is working on the search page using CommunityServicesAndDevelopment
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
		Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                     | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Community Services And Development     | Aged And Disability Support            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     | Child Welfare                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     | Community Development                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     | Employment Services                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     | Fundraising                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     | Housing And Homelessness Services      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     |  Indigenous And Multicultural Services | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     | Management                             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Community Services And Development     |  Volunteer Coordination And Support    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
