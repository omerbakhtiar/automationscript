﻿Feature: MarketingAndCommunication
Check if the External links of the employer profile on Index Page
		is working as expected of MarketingAndCommunication

@MarketingAndCommunication
Scenario Outline: Check External Links of the employer is working on the search page using MarketingAndCommunication
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                      | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Marketing And Communications           | Brand Management                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Digital And Search Marketing            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Direct Marketing And CRM                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Event Management                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Internal Communications                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Management                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Market Research And Analysis            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Marketing Assistants And Coordinators   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Marketing Communications                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Product Management And Development      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Public Relations And Corporate Affairs  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Marketing And Communications           | Trade Marketing                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |

