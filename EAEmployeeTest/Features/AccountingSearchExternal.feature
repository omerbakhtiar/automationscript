﻿Feature: ExternalSearchSalesAccounting
		Check if the External links of the employer profile on Index Page
		is working as expected

@Acccounting
Scenario Outline: Check External Links of the employer is working on the search page using sales category Accounting
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification         | Sub-Classification						 | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Accounts Officer And Clerk | Analysis And Reporting				     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Accounts Officer And Clerk				 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Assistant Accountants					 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Audit - External							 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Audit - Internal							 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Bookkeeping And Small Practices Accounting| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Business Services And Corporate Advisory  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Compliance And Risk                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Corporate Recovery                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Cost Accounting                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Financial Accounting And Reporting        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Financial Manager And Controller          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Investigation                             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Accounts Officer And Clerk | Management                                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Accounts Officer And Clerk |Management Accounting And Budgeting        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |


		



