﻿Feature: EmployerData
       In order to extract data from the
	   site so we can get their email address
	   to do the email blast
Scenario Outline: Fetching email from the directory site
		Given I have navigated to the application of the given URL of  http://www.businessdirectory.pk/
		Then I select the "<CityName>" as given by the user for the city of that place
		Then I select the name of the "<Category>" as given by the user
		Then I select the "<subcategory>" of the company
	Examples: 
	| CityName | Category | subcategory                  |
	| Karachi  | Textiles | Textile Agents				 | 