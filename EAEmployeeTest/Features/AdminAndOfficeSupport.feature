﻿Feature: AdminAndOfficeSupport
		Check if the External links of the employer profile on Index Page
		is working as expected of AdminOfficeSupport

@AdminAndOffice
Scenario Outline: Check External Links of the employer is working on the search page using Manufacturing, Transport And Logistics
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                      | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Administration And Office Support      | Administrative Assistants               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Administration And Office Support      | Client And Sales Administration         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Administration And Office Support      | Contracts Administration                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Administration And Office Support      | Data Entry And Word Processing          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Administration And Office Support      | Office Management                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Administration And Office Support      | Receptionists                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Administration And Office Support      | Records Management And Document Control | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
