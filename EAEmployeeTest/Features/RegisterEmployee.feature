﻿Feature: RegisterEmployee
	Check if the Register of Employee functionality is working
	as expected with valid data

@smoke @negative
Scenario Outline: Check Register of employee with  valid data
	Given I have navigated to the application
	And I close the popup
	Then I click register link
	Then I am on the Register Page
	Then I enter invalid data in "<FirstName>","<LastName>","<MobileNumber>","<email>","<password>","<confirmPassword>","<MainClasssification>" and "<Subclassification>"
	Then I click on accept application
	Then I submit the form
	Then I should be Navigated to Verification Code Page and I enter vefication code there
	Then I submit the button 
	Then I close the application
	Examples: 
	| FirstName | LastName | MobileNumber | email                             | password | confirmPassword | MainClasssification | Subclassification          | 
	| ahmed     | ali      | 03224258209  | ahmd1234@gmail.com                | omer123  | omer123         | Accounting          | Accounts Officer And Clerk |

	