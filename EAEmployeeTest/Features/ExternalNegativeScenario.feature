﻿Feature: ExternalNegativeScenario
		Check if the External Search functionality is working
	as expected with invalid data

@smoke @positive
Scenario Outline: Check External Search with invalid data in scenario outline
	Given I have navigated to the application
	And I close the popup
	And I enter "<text>" in external search
	And I select "<city>" from the drop down appearing on the page
	Then i select "<industry>" from the drop down on that page
	Then I click on find job button to searcch the job
	Then I verify if no search result is found
	Then I click on the Job
	Then I close the application
	Examples: 
    | text                                                          | city      | industry                                 |
    |                                                               |           | Information And Communication Technology |
    |                                                               |           |                                          |
    |                                                               |           | Marketing And Communications             |
    |                                                               | Karachi   | Marketing And Communications             |
    |                                                               | Islamabad | Sales                                    |
    |                                                               | Quetta    | Information And Communication Technology |
    |                                                               |           | Sales                                    |
    |                                                               |           | Accounting                               |
    |                                                               |           | Design And Architecture                  |
    |                                                               |           | Engineering                              |
    |                                                               |           | Human Resources And Recruitment          |
    |                                                               |           | Education And Training                   |
    |                                                               |           | Administration And Office Support        |
    |                                                               |           | Manufacturing, Transport And Logistics   |
    |                                                               |           | Agriculture                              |
    |                                                               |           | Hospitality And Tourism                  |
    |                                                               | Peshawar  | Accounting                               |
    | Members, Support Groups and Volunteer Coordinator/Group/Lover | Peshawar  | Community Services And Development       |
    |                                                               | Peshawar  |                                          |
    | Incharge Service Center - Multan                              | Multan    | Calls Center                             |
    | 10 Test Job                                                   | Islamabad | Banking And Finance services             |
    |                                                               | Islamabad |                                          |
    | ////+++-----4334                                              |           |                                          |
    | //                                                            |           |                                          |
    | +++                                                           |           |                                          |
    | ...                                                           |           |                                          |
    | ()                                                            |           |                                          |
    | ===--                                                         |           |                                          |
    | *                                                             |           |                                          |
    | http://www.geekinterview.com/question_details/59703           |           |                                          |