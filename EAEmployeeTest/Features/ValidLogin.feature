﻿Feature: ValidLogin
	Check if the login functionality is working
	as expected with valid data

@new 
Scenario Outline: Check Login with  valid username and password with scenario outline
	Given I have navigated to the application
	And I close the popup
	Then I click login link
	When I enter "<UserName>"and"<Password>" with integer values
    Then I am on the HomePage of the employee
    Then I close the application
	Examples: 
	| UserName         | Password |
	| omerbk@gmail.com | 123456   |  