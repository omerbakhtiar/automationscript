﻿Feature: ExternalIT
		Check if the External links of the employer profile on Index Page
		is working as expected

@multipleSubclassification
Scenario Outline: Check External Links of the employer is working on ExternalIT
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select again "<MainClassification>","<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and "<update>"
	Examples:
            | MainClassification                       | Sub-Classification                                                                                                                                     | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type | update |
            | Information And Communication Technology | Developers And Programmers;Application Support And Administration;Artificial Intelligence;Help Desk And IT Support;Networks And Systems Administration | NA       | NA     | NA  | NA                | NA                  | NA          | NA        | test   |
            | Healthcare And Medical                   | Ambulance And Paramedics                                                                                                                               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        | NA     |
			| Healthcare And Medical                   | Ambulance And Paramedics;Chiropractic And Osteopathic;Dental                                                                                           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        | NA     |