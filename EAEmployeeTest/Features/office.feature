﻿Feature: office
	Check if the Register of Employer functionality is working
	as expected with different invalid data

@smoke @negative
Scenario Outline: Check Register of employer with  Invalid data
	Given I have navigated to the application
	And I close the popup
	Then I click register link
	Then I click on buisness link
	Then I enter "<EmployerInitial>","<EmployerLast>","<EmployerEmail>","<CNIC>","<emppassword>"and"<empconfirmpassword>"
	Then I click Next button
	Then I check that invalid accurate "<messages>" is appearing
	Then BuisnessName is not displayed as page is not navigated forward
	Then I close the application
	#first name and last name error message to be changed that only alphabets and space is allowed
	Examples: 
	| EmployerInitial | EmployerLast | EmployerEmail  | CNIC             | emppassword | empconfirmpassword | messages                     |
	| Emp             | First123     | emp3@gmail.com | 1233355498908003 | omer123     | omer123            | Please fill mandatory fields |
	| Emp12           | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            | Please fill mandatory fields |
	| Emp             | First        | emp2@gmail.com | 1233fdddfdedffdf | omer123     | omer123            | Only digits are allowed      |
	| Emp             | First        | emp2           | 1233355498908003 | omer123     | omer123            | Email address is invalid     |
	| Emp             | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123dt          | Passwords should match       |
	| Emp             | First        | emp3@gmail.com | 1233355498908003 | omer1231    | omer123            | Passwords should match       |
	| Emp             | First        |                | 1233355498908003 | omer123     | omer123            | Email/Username is mandatory. |
	| Emp             |              | emp3@gmail.com | 1233355498908003 | omer123     | omer123            | Please fill mandatory fields |
	|                 | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            | Please fill mandatory fields |
	| Emp             | First        | emp3@gmail.com |                  | omer123     | omer123            | CNIC is mandatory            |
	| Emp             | First        | emp3@gmail.com | 1231321545661221 | omer123     |                    | Passwords should match       |
	| Emp             | First        | emp3@gmail.com | 1231321545661221 |             | omer123            | Passwords should match       |



