﻿Feature: ExrnalSearchTest
		Check if the External links of the employer profile on Index Page
		is working as expected of Manufacturing, Transport And Logistics

@Manufacturing
Scenario Outline: Check External Links of the employer is working on the search page using Test
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification     | Sub-Classification                 | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Science And Technology | Biotechnology And Genetics         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Science And Technology | Biological And Biomedical Sciences | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Science And Technology | Chemistry And Physics              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Science And Technology | Quality Assurance And Control      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Science And Technology | Environmental                      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Science And Technology |Food Technology And Safety          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Science And Technology |Laboratory And Technical Services   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Science And Technology |Materials Sciences                  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Science And Technology |Mathematics                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Science And Technology |Modelling And Simulation            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |

