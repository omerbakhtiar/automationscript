﻿Feature: BankingAndFinanceServicesExternal
		Check if the External links of the employer profile on Index Page
		is working as expected of BankingAndFinanceServicesExternal

@BankingAndFinanceServicesExternal
Scenario Outline: Check External Links of the employer is working on the search page using BankingAndFinanceServicesExternal 
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and RetailExtenalSearch
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification           | Sub-Classification						| Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Banking And Finance Services | Accounts And Relationship Management	| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Banking And Finance Services |Analysis Reporting						| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Banking And Finance Services | Banking - Business						| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Banking And Finance Services |Banking - Corporate And Institutional	| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Banking And Finance Services |Banking - Retail Or Branch				| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Banking And Finance Services | Client Services						| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Collections							| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Compliance And Risk					| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  |Corporate Finance And Investment Banking| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Collections							| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  |Customer Service - Call Centre          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Financial Planning                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Funds Management                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    |Banking And Finance Services  | Management                             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Mortgages                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    |Banking And Finance Services  | Settlements                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Stockbroking And Trading               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    |Banking And Finance Services  | Takaful                                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Treasury                               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			|Banking And Finance Services  | Stockbroking And Trading               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		