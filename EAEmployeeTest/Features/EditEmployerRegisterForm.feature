﻿Feature: EditEmployerRegisterForm
         Check whether the update form is working of the employer create profile
		 as expected
	Scenario Outline: Check Register of employer update form is working as expected
	Given I have navigated to the application
	And I close the popup
	Then I click register link
	Then I click on buisness link
	Then I enter "<EmployerInitial>","<EmployerLast>","<EmployerEmail>","<CNIC>","<emppassword>"and"<empconfirmpassword>"
	Then I click Next button
	Then I click previous button
	Then I edit the fields of first form of the employer registeration
	Then I enter "<EmployerInitial>","<EmployerLast>","<EmployerEmail>","<CNIC>","<emppassword>"and"<empconfirmpassword>"
	Then I click Next button
	Then I enter second form data "<BuisnessName>","<BuisnessType>","<BuisnessURL>","<MobileNumber>"and"<IndustrialClassification>"
	Then I click on second Next button
	Then I enter the city "<City>"
	Then I verify that "<City>" ,"<state>" and "<country>" is autoselected
	Then I enter "<Adress1>","<Address2>" and "<postCode>"
	Then I edit up the adress fields
	Then I renter the edit fields of adress "<Adress1>","<Address2>" and "<postCode>"
	Then I tick the acccept button
	Then I submit employer form
	Then  I enter up the code
	Then I submit the button of the employer
	Then I close the application
	Examples: 
	| EmployerInitial | EmployerLast | EmployerEmail  | CNIC             | emppassword | empconfirmpassword | BuisnessName | BuisnessType          | BuisnessURL   | MobileNumber | IndustrialClassification          | secondmessages               | City    | state | country  | Adress1      | Address2       | postCode |
	| Emp             | First        | emp0@gmail.com | 1233355498908003 | omer123     | omer123            | Test         | Unlimited Proprietary | www.afrah.com | 03224258209  | Administration And Office Support | Please fill mandatory fields | KARACHI | SINDH | PAKISTAN | test address | second address | 5400     |