﻿Feature: MiningExternalSearch
	Check if the External links of the employer profile on Index Page
	is working as expected of MiningExternalSearch

@MiningExternalSearch
Scenario Outline: Check External Links of the employer is working on the search page using MiningExternalSearch
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                 | Sub-Classification								| Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Mining, Resources And Energy       |Analysis And Reporting			                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       |Health, Safety And Environment			        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       | Management			                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       | Mining - Drill And Blast			                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Mining, Resources And Energy       |Mining - Engineering And Maintenance		        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Mining, Resources And Energy       |Mining - Exploration And Geoscience		        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Mining, Resources And Energy       | Mining - Operations			                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       |Mining - Processing								| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Mining, Resources And Energy       |Natural Resources And Water                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Mining, Resources And Energy       |Oil And Gas - Drilling		                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       |Oil And Gas - Engineering And Maintenance		    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       |Oil And Gas - Exploration And Geoscience		    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       |Oil And Gas - Operations	                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       |Oil And Gas - Production And Refinement	        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Mining, Resources And Energy       |Power Generation And Distribution                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Mining, Resources And Energy       |Surveying                                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
