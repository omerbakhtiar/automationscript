﻿Feature: EmployerSecondFormInvalid 
	@smoke @negative
Scenario Outline: Check Register of employer with  Invalid data  on second form
	Given I have navigated to the application
	And I close the popup
	Then I click register link
	Then I click on buisness link
	Then I enter "<EmployerInitial>","<EmployerLast>","<EmployerEmail>","<CNIC>","<emppassword>"and"<empconfirmpassword>"
	Then I click Next button
	Then I enter second form data "<BuisnessName>","<BuisnessType>","<BuisnessURL>","<MobileNumber>"and"<IndustrialClassification>"
	Then I click on second Next button
	Then I check that invalid accurate on second form "<secondmessages>" is appearing
	Then I close the application
	Examples: 
	| EmployerInitial | EmployerLast | EmployerEmail  | CNIC             | emppassword | empconfirmpassword | BuisnessName | BuisnessType          | BuisnessURL   | MobileNumber | IndustrialClassification          | secondmessages                                          |
    | Emp             | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            |              | Unlimited Proprietary | www.afrah.com | 03224258209  | Administration And Office Support |  Please fill mandatory fields                           |
	| Emp             | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            | Test         | Unlimited Proprietary | test          | 03214310072  | Administration And Office Support |  Please fill mandatory fields                           |
    | Emp             | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            | Test         |                       | test          | 03214310072  | Administration And Office Support |  Please fill mandatory fields                           |
	| Emp             | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            | Test         | Unlimited Proprietary | www.afrah.com | 03224258209  |                                   |  Please fill mandatory fields                           |
    | Emp             | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            |              |                       |               | 03224258209  |                                   |  Please fill mandatory fields                           |
    | Emp             | First        | emp3@gmail.com | 1233355498908003 | omer123     | omer123            |              |                       |               |  1           |                                   |  Please fill mandatory fields                           |
