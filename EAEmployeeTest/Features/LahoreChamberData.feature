﻿Feature:Extract Data From Lahore Chamber
	    Script is being developed to get
		data from lahore chamber of commerce sites
		to get information for email marketing

Scenario Outline: Fetching companies information  from the site of the Lahore Chamber of Commerce
		Given I have navigated to the application of the given URL of the lahore chamber of commerce
		Then I click on the advance button
		Then I select the "<BuisnessType>" and "<BuisnessSector>" as given by the user who is entering up the data
		Then I click on the button
	Examples: 
	| BuisnessType | BuisnessSector |
	| EXPORTERS    | TEXTILE        |