﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.0.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace EAEmployeeTest.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("AdminAndOfficeSupport")]
    public partial class AdminAndOfficeSupportFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "AdminAndOfficeSupport.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "AdminAndOfficeSupport", "\t\tCheck if the External links of the employer profile on Index Page\r\n\t\tis working" +
                    " as expected of AdminOfficeSupport", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Check External Links of the employer is working on the search page using Manufact" +
            "uring, Transport And Logistics")]
        [NUnit.Framework.CategoryAttribute("AdminAndOffice")]
        [NUnit.Framework.TestCaseAttribute("Administration And Office Support", "Administrative Assistants", "NA", "NA", "NA", "NA", "NA", "NA", "NA", new string[0])]
        [NUnit.Framework.TestCaseAttribute("Administration And Office Support", "Client And Sales Administration", "NA", "NA", "NA", "NA", "NA", "NA", "NA", new string[0])]
        [NUnit.Framework.TestCaseAttribute("Administration And Office Support", "Contracts Administration", "NA", "NA", "NA", "NA", "NA", "NA", "NA", new string[0])]
        [NUnit.Framework.TestCaseAttribute("Administration And Office Support", "Data Entry And Word Processing", "NA", "NA", "NA", "NA", "NA", "NA", "NA", new string[0])]
        [NUnit.Framework.TestCaseAttribute("Administration And Office Support", "Office Management", "NA", "NA", "NA", "NA", "NA", "NA", "NA", new string[0])]
        [NUnit.Framework.TestCaseAttribute("Administration And Office Support", "Receptionists", "NA", "NA", "NA", "NA", "NA", "NA", "NA", new string[0])]
        [NUnit.Framework.TestCaseAttribute("Administration And Office Support", "Records Management And Document Control", "NA", "NA", "NA", "NA", "NA", "NA", "NA", new string[0])]
        public virtual void CheckExternalLinksOfTheEmployerIsWorkingOnTheSearchPageUsingManufacturingTransportAndLogistics(string mainClassification, string sub_Classification, string location, string gender, string age, string minimumEducation, string yearsOfExperience, string dateListed, string workType, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "AdminAndOffice"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check External Links of the employer is working on the search page using Manufact" +
                    "uring, Transport And Logistics", @__tags);
#line 6
this.ScenarioSetup(scenarioInfo);
#line 7
 testRunner.Given("I have navigated to the application", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 8
 testRunner.And("I close the popup", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 9
 testRunner.Then("I click on find job button to searcch the job", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 10
 testRunner.Then(string.Format("I select \"{0}\" and \"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\" and \"{8}\" and click " +
                        "update search and verify results count", mainClassification, sub_Classification, location, gender, age, minimumEducation, yearsOfExperience, dateListed, workType), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 11
 testRunner.Then("I verify the count", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 12
 testRunner.Then("I close the application", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
