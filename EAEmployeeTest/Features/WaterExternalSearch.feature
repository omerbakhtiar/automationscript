﻿Feature: WaterExternalSearch
Check if the External links of the employer profile on Index Page
	is working as expected of WaterIndustry

@WaterIndustry
Scenario Outline: Check External Links of the employer is working on the search page using WaterIndustry
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                 | Sub-Classification								| Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Water Industry                     |Operations Handler			                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Water Industry                     |Wastewater Treatment			                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Water Industry                     |Water Supply		                                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |