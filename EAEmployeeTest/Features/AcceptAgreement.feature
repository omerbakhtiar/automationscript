﻿Feature: AcceptAgreement
	Check if the Register of Employee functionality is working
	as expected with different invalid data

 @negative @ahmed
Scenario Outline: Check Register of employee with  Invalid data
	Given I have navigated to the application
	And I close the popup
	Then I click register link
	Then I am on the Register Page
	Then I enter invalid data in "<FirstName>","<LastName>","<MobileNumber>","<email>","<password>","<confirmPassword>","<MainClasssification>" and "<Subclassification>"
	Then I submit the form
	Then I validate email error message "<message>" is displayed on employee registeration Page	
	Then I close the application
	Examples: 
	| FirstName | LastName | MobileNumber | email                             | password | confirmPassword | MainClasssification | Subclassification          | message                                                 |
	| ahmed     | ali      | 03224258209  | ahmed123@gmail.com                | omer123  | omer123         | Accounting          | Accounts Officer And Clerk |Please accept the terms and conditions before proceeding |

	