﻿Feature: RetailExtenalSearch
	Check if the External links of the employer profile on Index Page
		is working as expected of RetailExtenalSearch

@RetailExnternalSearch
Scenario Outline: Check External Links of the employer is working on the search page using Manufacturing, Transport And Logistics
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                     | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Retail And Consumer Products           | Buying                                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Retail And Consumer Products           | Management - Area And Multi-Site       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Retail And Consumer Products           | Management - Department And Assistant  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Retail And Consumer Products           | Management - Store                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Retail And Consumer Products           | Merchandisers                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Retail And Consumer Products           |Planning                                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Retail And Consumer Products           |Retail Assistants                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |


