﻿Feature: facebook
	Check is the facebook groups are getting added up or
	not 

Scenario Outline: Adding up the facebook groups 
	Given I have navigated to the facebook application
	When I enter "<UserName>"and"<Password>" for the facebook login
    Then I search with the "<SearchValue>"
	Examples: 
	| UserName                | Password       | SearchValue				   |
	| need4designer@yahoo.com | pakistan@2017* |Jobs In Lahore & Business      |