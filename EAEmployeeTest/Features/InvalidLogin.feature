﻿Feature: InvalidLogin
	Check if the login functionality is working
	as expected with different invalid data


@smoke @negative
Scenario Outline: Check Login with  Invalid username and password with scenario outline
	Given I have navigated to the application
	And I close the popup
	Then I click login link
	When I enter "<UserName>"and"<Password>" with integer values
	Then I am on not on homepage a i have entered invalid data
    Then I close the application
	Examples: 
	| UserName         | Password |
	| omerbk@gmail.com | 123409   |
	| ""               | 12345    |
	| omerbk@gmail.com | 123456   |
	
	
