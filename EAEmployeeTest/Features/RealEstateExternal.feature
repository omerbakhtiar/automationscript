﻿Feature: RealEstateExternal
Check if the External links of the employer profile on Index Page
		is working as expected of RealEstatae

@RealEstate
Scenario Outline: Check External Links of the employer is working on the search page using RealEstate
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification							| Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Real Estate And Property               |Administration								| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Real Estate And Property               |Analysts									    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Real Estate And Property               |Body Corporate And Facilities Management		| NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Real Estate And Property               |Commercial Sales, Leasing And Property Mgmt   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Real Estate And Property               |Residential Leasing And Property Management   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Real Estate And Property               |Retail And Property Development	            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Real Estate And Property               |Valuation						                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			