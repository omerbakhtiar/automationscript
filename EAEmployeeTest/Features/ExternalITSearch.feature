﻿Feature: ExternalSearchSecondPage
		Check if the External links of the employer profile on Index Page
		is working as expected

@smoke @positive
Scenario Outline: Check External Links of the employer is working on Index page
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                       | Sub-Classification                     | Location | Gender     | Age   | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Information And Communication Technology | Developers And Programmers             | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
            | Information And Communication Technology | Application Support And Administration | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Computer Operators                     | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Consultants                            | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
		    | Information And Communication Technology | Artificial Intelligence                | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Engineering - Hardware                 | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Engineering - Network                  | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Help Desk And IT Support               | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Engineering - Software                 | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Product Management And Development     | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology | Management                             | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Networks And Systems Administration     | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Programme And Project Management        | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Sales - Pre And Post                    | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Security                                | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |System And Application Support          | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Team Leaders                            | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Technical Writing                       | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Telecommunications                      | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			| Information And Communication Technology |Testing And Quality Assurance           | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
		    | Information And Communication Technology |Web Development And Production          | NA       | NA         | NA    | NA                | NA                  | NA          | NA        |
			