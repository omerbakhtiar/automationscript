﻿Feature: ExrnalSearchEngineeringManufacturing
		Check if the External links of the employer profile on Index Page
		is working as expected of Manufacturing, Transport And Logistics

@Manufacturing
Scenario Outline: Check External Links of the employer is working on the search page using Manufacturing, Transport And Logistics
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                     | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | Manufacturing, Transport And Logistics | Analysis And Reporting                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Assembly And Process Work              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Aviation Services                      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Couriers, Drivers And Postal Services  | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Fleet Management                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Freight And Cargo Forwarding           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Import, Export And Customs             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Machine Operators                      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Pattern Makers And Garment Technicians | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics | Pickers And Packers                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | Manufacturing, Transport And Logistics |Production, Planning And Scheduling     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Manufacturing, Transport And Logistics |Public Transport And Taxi Services      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		    | Manufacturing, Transport And Logistics |Purchasing, Procurement And Inventory   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Manufacturing, Transport And Logistics |Quality Assurance And Control           | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Manufacturing, Transport And Logistics |Rail Transport                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Manufacturing, Transport And Logistics |Road Transport                          | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Manufacturing, Transport And Logistics |Team Leaders And Supervisors            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| Manufacturing, Transport And Logistics |Warehousing, Storage And Distribution   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |

	