﻿Feature: HealthCareAndMedicalSearch
	Check if the External links of the employer profile on Index Page
		is working as expected of HealthCareAndMedicalSearch

@HealthCareAndMedicalSearch
Scenario Outline: Check External Links of the employer is working on the search page using HealthCareAndMedicalSearch
	Given I have navigated to the application
	And I close the popup
	Then I click on find job button to searcch the job
	Then I select "<MainClassification>" and "<Sub-Classification>","<Location>","<Gender>","<Age>","<Minimum Education>","<Years of Experience>","<Date Listed>" and "<Work Type>" and click update search and verify results count
	Then I verify the count
	Then I close the application
	Examples:
            | MainClassification                     | Sub-Classification                         | Location | Gender | Age | Minimum Education | Years of Experience | Date Listed | Work Type |
            | HealthCareAndMedicalSearch             | Ambulance And Paramedics                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Chiropractic And Osteopathic               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Clinical And Medical Research              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Dental                                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Dieticians                                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Environmental Services                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | General Practitioners                      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Health N.E.C.                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Management                                 | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Medical Administration                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Medical Imaging                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Medical Specialists                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Natural Therapies And Alternative Medicine | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Nursing - AAndE                            | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Nursing - Aged Care                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             | Nursing - Community                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Nursing - Educators And Facilitators        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Nursing - General Medical And Surgical      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Nursing - High Acuity                       | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Nursing - Management                        | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | HealthCareAndMedicalSearch             |Nursing - Midwifery                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Nursing - Pediatric And PICU                | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | HealthCareAndMedicalSearch             |Nursing - Psych                             | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Nursing - Theatre And Recovery              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
            | HealthCareAndMedicalSearch             |Nursing - Midwifery                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Optical                                     | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Outpatient Services                         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Pathology                                   | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Pharmaceuticals And Medical Devices         | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Pharmacy                                    | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Physiotherapy                               | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |Public Health Services                      | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
			| HealthCareAndMedicalSearch             |	RAndD Health                              | NA       | NA     | NA  | NA                | NA                  | NA          | NA        |
		



			